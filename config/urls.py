# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView
from django.views import defaults as default_views

urlpatterns = [
    url(r'^$', TemplateView.as_view(template_name='pages/home.html'), name='home'),
    url(r'^about/$', TemplateView.as_view(template_name='pages/about.html'), name='about'),

    # Django Admin, use {% url 'admin:index' %}
    url(settings.ADMIN_URL, include(admin.site.urls)),

    # User management
    url(r'^users/', include('custofeed.users.urls', namespace='users')),
    url(r'^accounts/', include('allauth.urls')),

    # DJANGO REST Auth
    url(r'^rest-auth/', include('rest_auth.urls')),
    url(r'^rest-auth/registration/', include('rest_auth.registration.urls')),


    # LOCAL APPS ---------------

    # feedbacks app
    url(r'^feedbacks/', include('custofeed.feedbacks.urls', namespace='feedbacks')),

    # dashboard app
    url(r'^dashboard/', include('custofeed.dashboard.urls', namespace='dashboard')),

    # mails app
    url(r'^mails/', include('custofeed.mails.urls', namespace='mails')),

    # loyalty app
    url(r'^loyalty/', include('custofeed.loyalty.urls', namespace='loyalty')),

    # staff app
    url(r'^staff/', include('custofeed.staff.urls', namespace='staff')),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    # URLs allowed during development only
    urlpatterns += [
        # This allows the error pages to be debugged during development
        url(r'^400/$', default_views.bad_request, kwargs={'exception': Exception('Bad Request!')}),
        url(r'^403/$', default_views.permission_denied, kwargs={'exception': Exception('Permission Denied')}),
        url(r'^404/$', default_views.page_not_found, kwargs={'exception': Exception('Page not Found')}),
        url(r'^500/$', default_views.server_error),

        # Django REST Framework
        url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

        # DJANGO REST Auth
        url(r'^docs/', include('rest_framework_docs.urls')),
    ]
