////////////////////////////////
//Setup//
////////////////////////////////
// Plugins
var gulp = require('gulp')
    , es6 = require('es6-promise').polyfill()
    , pjson = require('./package.json')
    , gutil = require('gulp-util')
    , sass = require('gulp-sass')
    , autoprefixer = require('gulp-autoprefixer')
    , cssnano = require('gulp-cssnano')
    , rename = require('gulp-rename')
    , del = require('del')
    , plumber = require('gulp-plumber')
    , pixrem = require('gulp-pixrem')
    , uglify = require('gulp-uglify')
    //, imagemin = require('gulp-imagemin')
    
    , exec = require('gulp-exec')
    , runSequence = require('run-sequence')
    , browserSync = require('browser-sync');
// Relative paths function
var pathsConfig = function (appName) {
    this.app = "./" + (appName || pjson.name);
    return {
        app: this.app
        , templates: this.app + '/templates'
        , sass: this.app + '/static/sass'
        , fonts: this.app + '/static/fonts'
        , images: this.app + '/static/images'
        , js: this.app + '/static/js'
        , jsLibs: this.app + '/static/js-libs'
        , docs: this.app + '/static/docs'
        , destCss: this.app + '/dist/css'
        , destJs: this.app + '/dist/js'
        , destJsLibs: this.app + '/dist/js/libs'
        , destImages: this.app + '/dist/images'
        , destFonts: this.app + '/dist/fonts'
        , ddIcons: this.app + '/static/sass/dashboard/dd-icon'
        , destDdIcons: this.app + '/dist/css/dashboard/dd-icon'
        , destDocs: this.app + '/dist/docs'
    , }
};
var paths = pathsConfig();
////////////////////////////////
//Tasks//
////////////////////////////////
// Styles autoprefixing and minification
gulp.task('styles', function () {
    return gulp.src(paths.sass + '/**/*.scss').pipe(sass().on('error', sass.logError)).pipe(plumber()) // Checks for errors
        .pipe(autoprefixer({
            browsers: ['last 2 version']
        })) // Adds vendor prefixes
        .pipe(pixrem()) // add fallbacks for rem units
        .pipe(rename({
            suffix: '.min'
        })).pipe(cssnano()) // Minifies the result
        .pipe(gulp.dest(paths.destCss));
});
// Javascript minification
gulp.task('scripts', function () {
    return gulp.src(paths.js + '/**/*.js').pipe(plumber()) // Checks for errors
        .pipe(uglify()) // Minifies the js
        .pipe(rename({
            suffix: '.min'
        })).pipe(gulp.dest(paths.destJs));
});
// Javascript libraries
gulp.task('scripts-libs', function () {
    return gulp.src(paths.jsLibs + '/**/*.js').pipe(plumber()) // Checks for errors
        .pipe(gulp.dest(paths.destJsLibs));
});
// Image compression
gulp.task('imgCompression', function () {
    return gulp.src(paths.images + '/**/*')
        //        .pipe(imagemin({
        //            verbose: true
        //        })) // Compresses PNG, JPEG, GIF and SVG images
        .pipe(gulp.dest(paths.destImages))
});
// Fonts to dist folder
gulp.task('fonts', function () {
    return gulp.src(paths.fonts + '/**/*'). //Tranfer fonts to dist folder
    pipe(gulp.dest(paths.destFonts))
});
// DD Icons to dist folder
gulp.task('ddIcons', function () {
    return gulp.src(paths.ddIcons + '/**/*'). //Tranfer fonts to dist folder
    pipe(gulp.dest(paths.destDdIcons))
});
// Fonts to dist folder
gulp.task('docs', function () {
    return gulp.src(paths.docs + '/**/*'). //Tranfer docs to dist folder
    pipe(gulp.dest(paths.destDocs))
});
// Run django server
gulp.task('runServer', function () {
    exec('python manage.py runserver', function (err, stdout, stderr) {
        console.log(stdout);
        console.log(stderr);
    });
});
// Browser sync server for live reload
gulp.task('browserSync', function () {
    browserSync.init(
        [paths.sass + "/**/*.scss", paths.js + "/**/*.js", paths.jsLibs + "/**/*.js", paths.templates + '/**/*.html'], {
            proxy: "localhost:8000"
        });
});
// Default task
gulp.task('default', function () {
    runSequence(['styles', 'scripts-libs', 'scripts', 'imgCompression', 'fonts', 'ddIcons', 'docs'], 'runServer', 'browserSync');
});
////////////////////////////////
//Watch//
////////////////////////////////
// Watch
gulp.task('watch', ['default'], function () {
    gulp.watch(paths.sass + '/**/*.scss', ['styles']);
    gulp.watch(paths.jsLibs + '/**/*.js', ['scripts-libs']);
    gulp.watch(paths.js + '/**/*.js', ['scripts']);
    gulp.watch(paths.images + '/**/*', ['imgCompression']);
    gulp.watch(paths.fonts + '/**/*', ['fonts']);
    gulp.watch(paths.ddIcons + '/**/*', ['ddIcons']);
    gulp.watch('templates/**/*.html');
});