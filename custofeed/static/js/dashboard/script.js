var heightExtra = 35;
var retentionChart = 'chart';
var satisfactionChart = 'chart1';
var categoryChart = 'chart2';
var itemStyle = {
    normal: {
        lineStyle: {
            width: 3
        }
    }
}
$(function () {
    init();
    //INPUT GROUP -----
    $('input').focus(function () {
        $(this).parent().addClass('focused');
    });
    $('input').blur(function () {
        $(this).parent().removeClass('focused');
    });
    //FORM-WIZARD ----
    $('.formWizardNext').click(function () {
        var carousel = $($(this).data('target'));
        var form = carousel.find('.item.active form');
        form.validate();
        // prepare Options Object 
        var options = {
            beforeSubmit: function (arr, $form, options) {
                $form.parent().parent().parent().append('<div class="loader"><div class="spinner"></div></div>');
            }
            , success: function (data) {
                if (data.response == 'success') {
                    $('.loader').fadeOut();
                    carousel.carousel('next');
                    if (data.message) {
                        notify(data.message);
                    }
                }
                else if (data.response == "redirect") {
                    notify('Redirecting ...');
                    window.location.href = data.message;
                }
                else if (data.response == "error") {
                    if (data.message) {
                        notify(data.message, 'danger');
                    }
                    $('.loader').fadeOut();
                }
                else {
                    notify('Error!', 'danger');
                    $('.loader').fadeOut();
                }
                if ($('.carousel-inner .item:last').hasClass('active')) {
                    window.location.href = $('#redirectLink').val();
                }
            }
        };
        form.ajaxForm(options);
        if (form.valid()) {
            form.submit();
            return false;
        }
    });
    $(".ajaxSubmit").click(function (event) {
        var target = $(this).data('target');
        showLoader($('body'));
        $.ajax({
            type: "POST"
            , url: $(this).data('url')
            , data: {
                csrfmiddlewaretoken: getCookie('csrftoken')
                , submit: $(this).data('submit')
                , id: $(this).data('id')
                , val: $(this).data('val')
            }
            , statusCode: {
                404: function () {
                    notify('Error!', 'Page Not Found!');
                }
            }
        }).done(function (data) {
            if (data.response == 'success') {
                $(target).fadeOut();
                hideLoader($('body'));
                if (data.message) {
                    notify(data.message);
                }
            }
        }).fail(function (msg) {
            return false;
        });
    });
    //DATA TABLES ------
    try {
        $('#table').DataTable({
            dom: 'Blfrtip'
            , buttons: [
                {
                    extend: 'csv'
                    , text: 'Export All as CSV'
                }, {
                    extend: 'csv'
                    , text: 'Export Selected as CSV'
                    , exportOptions: {
                        modifier: {
                            selected: true
                        }
                    }
                }
            ]
            , select: true
            , "order": [[0, "desc"]]
        });
    }
    catch (err) {
        console.log('Data Tables not loaded');
    }
});
$(window).resize(function () {
    init();
});

function init() {
    $.ajaxSetup({
        cache: true
    });
    $('#chart').css('height', $('#chart').parent().parent().outerHeight() - $('#chartHeading').outerHeight() - 20);
    if ($('#chart').outerHeight() > 0) {
        $('#chart1').css('height', $('#chart').outerHeight());
        $('#chart2').css('height', $('#chart').outerHeight());
    }
    else {
        $('#chart1').css('height', 500);
        $('#chart2').css('height', 500);
    }
    //NEWSTRICKER ----
    var topComments = $('#topComments').newsTicker({
        row_height: ($('#chart').parent().parent().outerHeight() / 5) - (heightExtra / 2)
        , max_rows: 5
        , duration: 4000
    });
    var dobTicker = $('#dobTicker').newsTicker({
        row_height: ($('#chart').parent().parent().outerHeight() / 5) - (heightExtra / 2)
        , max_rows: 5
        , duration: 4000
    });
    var anniTicker = $('#anniTicker').newsTicker({
        row_height: ($('#chart').parent().parent().outerHeight() / 5) - (heightExtra / 2)
        , max_rows: 5
        , duration: 4000
    });
}

function notify(string, type) {
    if (type) {
        $('body').append('<div class="noti noti-' + type + '">' + string + '</div>');
    }
    else {
        $('body').append('<div class="noti">' + string + '</div>');
    }
}

function plotChart(type, url) {
    switch (type) {
    case 'N':
        showLoader($('#chart'));
        break;
    case 'S':
        showLoader($('#chart1'));
        break;
    case 'C':
        showLoader($('#chart2'));
        break;
    }
    $.getJSON(url, function (data) {
        $.getScript('/dist/js/libs/echarts.js', function () {
            switch (type) {
            case 'N':
                plotRetentionChart(data);
                break;
            case 'S':
                plotSatisfactionChart(data);
                break;
            case 'C':
                plotCategoryChart(data);
                break;
            }
        });
    });
}

function plotRetentionChart(data) {
    var x_tick = [];
    var promoters = [];
    var passives = [];
    var detractors = [];
    var total = [];
    $(data).each(function () {
        x_tick.push(this[0]);
        promoters.push(this[1]);
        passives.push(this[2]);
        detractors.push(this[3]);
        total.push(parseInt(this[1]) + parseInt(this[2]) + parseInt(this[3]))
    });
    //ECHARTS
    // use
    // Initialize after dom ready
    var myChart = echarts.init(document.getElementById('chart'), 'macarons');
    option = {
        color: ['#10cfbd', '#f8d053', '#f55753', '#E6E6E6']
        , tooltip: {
            trigger: 'axis'
        }
        , toolbox: {
            show: true
            , feature: {
                magicType: {
                    show: true
                    , type: ['line', 'bar', 'stack', 'tiled']
                    , title: {
                        line: 'Line'
                        , bar: 'Bar'
                        , stack: 'Stack'
                        , tiled: 'Tiled'
                    }
                }
                , restore: {
                    show: true
                    , title: 'Restore'
                }
                , saveAsImage: {
                    show: true
                    , title: 'Save as Image'
                }
            }
        }
        , calculable: true
        , legend: {
            data: ['Promoters', 'Passives', 'Detractors', 'Total']
        }
        , grid: {
            x: 50
            , x2: 50
        }
        , xAxis: [
            {
                type: 'category'
                , boundaryGap: true
                , axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'category'
                , data: x_tick
                    }
                ]
        , yAxis: [
            {
                axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'value'
                    }
                ]
        , series: [
            {
                name: 'Promoters'
                , type: 'line'
                , showAllSymbol: true
                , symbolSize: 10
                , data: promoters
                , smooth: true
                , itemStyle: itemStyle
                    }
                    , {
                name: 'Passives'
                , type: 'line'
                , showAllSymbol: true
                , symbolSize: 10
                , data: passives
                , smooth: true
                , itemStyle: itemStyle
                    }
                    , {
                name: 'Detractors'
                , type: 'line'
                , showAllSymbol: true
                , symbolSize: 10
                , data: detractors
                , smooth: true
                , itemStyle: itemStyle
                    }
                    , {
                name: 'Total'
                , type: 'line'
                , showAllSymbol: true
                , symbolSize: 0
                , data: total
                , smooth: true
                , itemStyle: {
                    normal: {
                        lineStyle: {
                            width: 0
                        }
                        , areaStyle: {
                            type: 'default'
                        }
                    }
                }
            }]
    };
    // Load data into the ECharts instance 
    myChart.setOption(option);
    $(window).resize(function () {
        myChart.resize();
    });
}

function plotSatisfactionChart(data) {
    var x_tick = [];
    var happy = [];
    var unhappy = [];
    $(data).each(function () {
        x_tick.push(this[0]);
        happy.push(this[1]);
        unhappy.push(this[2]);
    });
    //ECHARTS
    // Initialize after dom ready
    var myChart = echarts.init(document.getElementById(satisfactionChart), 'macarons');
    option = {
        color: ['#10cfbd', '#f55753']
        , tooltip: {
            trigger: 'axis'
        }
        , toolbox: {
            show: true
            , feature: {
                magicType: {
                    show: true
                    , type: ['line', 'bar', 'stack', 'tiled']
                    , title: {
                        line: 'Line'
                        , bar: 'Bar'
                        , stack: 'Stack'
                        , tiled: 'Tiled'
                    }
                }
                , restore: {
                    show: true
                    , title: 'Restore'
                }
                , saveAsImage: {
                    show: true
                    , title: 'Save as Image'
                }
            }
        }
        , grid: {
            x: 50
            , x2: 50
        }
        , calculable: true
        , legend: {
            data: ['Happy', 'Unhappy']
        }
        , xAxis: [
            {
                type: 'category'
                , boundaryGap: true
                , axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'category'
                , data: x_tick
                    }
                ]
        , yAxis: [
            {
                axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'value'
                    }
                ]
        , series: [
            {
                name: 'Happy'
                , type: 'bar'
                , showAllSymbol: true
                , symbolSize: 10
                , data: happy
                , stack: 's'
                , smooth: true
                , itemStyle: itemStyle
                        }
                        , {
                name: 'Unhappy'
                , type: 'bar'
                , showAllSymbol: true
                , symbolSize: 10
                , data: unhappy
                , stack: 's'
                , smooth: true
                , itemStyle: itemStyle
                        }
                    ]
    };
    // Load data into the ECharts instance 
    myChart.setOption(option);
    $(window).resize(function () {
        myChart.resize();
    });
}

function plotCategoryChart(data) {
    var categories = [];
    var vals = [];
    $(data).each(function () {
        $.each(this, function (key, value) {
            if (value[0] != null && value[1] != null) {
                categories.push(key);
                if (value[0] == 0 || value[1] == 0) {
                    vals.push(0);
                }
                else {
                    vals.push(Math.round((value[0] / value[1]) * 100) / 100);
                }
            }
        });
    });
    //ECHARTS
    // Initialize after dom ready
    var myChart = echarts.init(document.getElementById(categoryChart), 'macarons');
    option = {
        color: ['#10cfbd', '#f55753']
        , tooltip: {
            trigger: 'axis'
        }
        , toolbox: {
            show: true
            , feature: {
                restore: {
                    show: true
                    , title: 'Restore'
                }
                , saveAsImage: {
                    show: true
                    , title: 'Save as Image'
                }
            }
        }
        , calculable: true
        , legend: {
            data: ['Score']
        }
        , xAxis: [
            {
                axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'value'
            }
        ]
        , yAxis: [
            {
                type: 'category'
                , boundaryGap: true
                , axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , data: categories
            }
        ]
        , series: [
            {
                name: 'Score'
                , type: 'bar'
                , showAllSymbol: true
                , symbolSize: 10
                , data: vals
                , stack: 's'
                , smooth: true
                , itemStyle: itemStyle
            }
        ]
    };
    // Load data into the ECharts instance 
    myChart.setOption(option);
    $(window).resize(function () {
        myChart.resize();
    });
}

function plotMultiStoreChart(type, url) {
    switch (type) {
    case 'F':
        showLoader($('#multiStoreChart'));
        break;
    }
    $.getJSON(url, function (data) {
        $.getScript('/dist/js/libs/echarts.js', function () {
            switch (type) {
            case 'F':
                plotMultiStoreFeedbackChart(data);
                break;
            }
        });
    });
}

function plotSMSChart(type, url) {
    switch (type) {
    case 'P':
        showLoader($('#multiStoreChart'));
        break;
    }
    $.getJSON(url, function (data) {
        $.getScript('/dist/js/libs/echarts.js', function () {
            switch (type) {
            case 'P':
                plotMultiStoreFeedbackChart(data);
                break;
            }
        });
    });
}

function plotEmployeesChart(type, url) {
    switch (type) {
    case 'F':
        showLoader($('#multiStoreChart'));
        break;
    }
    $.getJSON(url, function (data) {
        $.getScript('/dist/js/libs/echarts.js', function () {
            switch (type) {
            case 'F':
                plotMultiStoreFeedbackChart(data);
                break;
            }
        });
    });
}

function plotMultiStoreFeedbackChart(data) {
    var x_tick = [];
    var total = [];
    var firstEle = true;
    var legend = data[0]
    var series = []
    for (i = 0; i < legend.length; i++) {
        total.push(Array());
    }
    $(data).each(function () {
        if (firstEle) {
            firstEle = false;
        }
        else {
            for (i = 0; i < legend.length; i++) {
                total[i].push(this[i + 1]);
            }
            x_tick.push(this[0]);
        }
    });
    for (i = 0; i < legend.length; i++) {
        series.push({
            name: legend[i]
            , type: 'bar'
            , showAllSymbol: true
            , symbolSize: 10
            , data: total[i]
            , smooth: true
            , itemStyle: itemStyle
        })
    }
    //ECHARTS
    // use
    // Initialize after dom ready
    var myChart = echarts.init(document.getElementById('multiStoreChart'), 'macarons');
    option = {
        tooltip: {
            trigger: 'axis'
        }
        , toolbox: {
            show: true
            , feature: {
                magicType: {
                    show: true
                    , type: ['line', 'bar', 'stack', 'tiled']
                    , title: {
                        line: 'Line'
                        , bar: 'Bar'
                        , stack: 'Stack'
                        , tiled: 'Tiled'
                    }
                }
                , restore: {
                    show: true
                    , title: 'Restore'
                }
                , saveAsImage: {
                    show: true
                    , title: 'Save as Image'
                }
            }
        }
        , calculable: true
        , legend: {
            data: legend
        }
        , grid: {
            x: 50
            , x2: 50
        }
        , xAxis: [
            {
                type: 'category'
                , boundaryGap: true
                , axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'category'
                , data: x_tick
            }
        ]
        , yAxis: [
            {
                axisLine: {
                    show: false
                }
                , axisTick: {
                    show: false
                }
                , type: 'value'
            }
        ]
        , series: series
    };
    // Load data into the ECharts instance 
    myChart.setOption(option);
    $(window).resize(function () {
        myChart.resize();
    });
}

function getDate(timestamp) {
    var date = new Date(timestamp);
    return date;
}
Date.prototype.addHours = function (h) {
    this.setHours(this.getHours() + h);
    return this;
}
Date.prototype.addMinutes = function (m) {
    this.setMinutes(this.getMinutes() + m);
    return this;
}
$(".chart-toggle").click(function () {
    $(this).parent().find(".chart-toggle").removeClass('btn-primary');
    $(this).addClass('btn-primary');
    var chart = $(this).data('chart');
    plotChart(chart, $(this).data('url'));
});
$(".chart-multistore-toggle").click(function () {
    $(this).parent().find(".chart-multistore-toggle").removeClass('btn-primary');
    $(this).addClass('btn-primary');
    var chart = $(this).data('chart');
    plotMultiStoreChart(chart, $(this).data('url'));
});
$(".chart-sms-toggle").click(function () {
    $(this).parent().find(".chart-sms-toggle").removeClass('btn-primary');
    $(this).addClass('btn-primary');
    var chart = $(this).data('chart');
    plotSMSChart(chart, $(this).data('url'));
});
$(".chart-employees-toggle").click(function () {
    $(this).parent().find(".chart-employees-toggle").removeClass('btn-primary');
    $(this).addClass('btn-primary');
    var chart = $(this).data('chart');
    plotEmployeesChart(chart, $(this).data('url'));
});

function showLoader(obj) {
    obj.append('<div class="loader"><div class="spinner"></div></div>')
}

function hideLoader(obj) {
    obj.find('.loader').fadeOut();
}
//Generating CSRF Token
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}