$(document).ready(function () {
    // Scroll Events
    $(window).scroll(function () {
        var wScroll = $(this).scrollTop();
        // Activate menu
        if (wScroll > 20) {
            $('#main-nav').addClass('active');
            $('#slide_out_menu').addClass('scrolled');
        }
        else {
            $('#main-nav').removeClass('active');
            $('#slide_out_menu').removeClass('scrolled');
        };
        //Scroll Effects
    });
    // Navigation
    $('#navigation').on('click', function (e) {
        e.preventDefault();
        $(this).addClass('open');
        $('#slide_out_menu').toggleClass('open');
        if ($('#slide_out_menu').hasClass('open')) {
            $('.menu-close').on('click', function (e) {
                e.preventDefault();
                $('#slide_out_menu').removeClass('open');
            })
        }
    });
    // Wow Animations
    wow = new WOW({
        boxClass: 'wow', // default
        animateClass: 'animated', // default
        offset: 0, // default
        mobile: true, // default
        live: true // default
    })
    wow.init();
    // Menu For Xs Mobile Screens
    if ($(window).height() < 450) {
        $('#slide_out_menu').addClass('xs-screen');
    }
    $(window).on('resize', function () {
        if ($(window).height() < 450) {
            $('#slide_out_menu').addClass('xs-screen');
        }
        else {
            $('#slide_out_menu').removeClass('xs-screen');
        }
    });
    //Smooth Scrolling
    function filterPath(string) {
        return string.replace(/^\//, '').replace(/(index|default).[a-zA-Z]{3,4}$/, '').replace(/\/$/, '');
    }
    var locationPath = filterPath(location.pathname);
    var scrollElem = scrollableElement('html', 'body');
    $('a[href*=#]').each(function () {
        var thisPath = filterPath(this.pathname) || locationPath;
        if (locationPath == thisPath && (location.hostname == this.hostname || !this.hostname) && this.hash.replace(/#/, '')) {
            var $target = $(this.hash)
                , target = this.hash;
            if (target) {
                var targetOffset = $target.offset().top;
                $(this).click(function (event) {
                    event.preventDefault();
                    $(scrollElem).animate({
                        scrollTop: targetOffset
                    }, 1000, function () {
                        location.hash = target;
                    });
                });
            }
        }
    });
    // use the first element that is "scrollable"
    function scrollableElement(els) {
        for (var i = 0, argLength = arguments.length; i < argLength; i++) {
            var el = arguments[i]
                , $scrollElement = $(el);
            if ($scrollElement.scrollTop() > 0) {
                return el;
            }
            else {
                $scrollElement.scrollTop(1);
                var isScrollable = $scrollElement.scrollTop() > 0;
                $scrollElement.scrollTop(0);
                if (isScrollable) {
                    return el;
                }
            }
        }
        return [];
    }
});