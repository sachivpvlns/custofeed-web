# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from model_utils.models import TimeStampedModel

from django.contrib.auth.models import AbstractUser
from django.core.urlresolvers import reverse
from django.utils.encoding import python_2_unicode_compatible
from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from custofeed.core.behaviours import MobileMixin
from custofeed.companies.models import Company, Store


@python_2_unicode_compatible
class User(AbstractUser, MobileMixin):

    def __str__(self):
        return self.username

    def get_absolute_url(self):
        return reverse('users:detail', kwargs={'username': self.username})


@python_2_unicode_compatible
class UserWorkProfile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    company = models.ForeignKey(Company)
    title = models.CharField(_("title"), max_length=255)
    is_registered = models.BooleanField(
        _('registered'),
        default=False,
        help_text=_(
            'Designates whether this user has completed the b2b registration process. '
        ),
    )
    access_store = models.ManyToManyField(Store, verbose_name=_("access to stores"), blank=True)

    def __str__(self):
        return self.user.get_full_name()


@python_2_unicode_compatible
class OTPUser(MobileMixin, TimeStampedModel):
    from custofeed.feedbacks.models import Feedbacker
    token = models.CharField(_("token"), max_length=255, blank=False, null=False)
    feedbacker = models.OneToOneField(Feedbacker, blank=False, null=False)

    def __str__(self):
        return self.mobile
