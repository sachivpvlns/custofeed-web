# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from .models import *


class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User


class MyUserCreationForm(UserCreationForm):

    error_message = UserCreationForm.error_messages.update({
        'duplicate_username': 'This username has already been taken.'
    })

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class UserWorkProfileInline(admin.TabularInline):
    model = UserWorkProfile
    fields = ['company', 'title', 'access_store', 'is_registered']


@admin.register(User)
class MyUserAdmin(AuthUserAdmin):
    form = MyUserChangeForm
    add_form = MyUserCreationForm
    fieldsets = (
            ('User Profile', {'fields': ('mobile',)}),
    ) + AuthUserAdmin.fieldsets
    list_display = ('username', 'mobile', 'is_superuser')
    search_fields = ['mobile']
    inlines = [UserWorkProfileInline, ]


@admin.register(UserWorkProfile)
class UserWorkProfileAdmin(admin.ModelAdmin):
    fields = ['user', 'company', 'title', 'access_store', 'is_registered']
    list_display = ['user', 'company', 'title', 'is_registered']
    list_filter = ['company', 'title', 'is_registered']


@admin.register(OTPUser)
class OTPUserAdmin(admin.ModelAdmin):
    list_display = ['token', 'mobile', 'feedbacker', 'created']

