# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel

from custofeed.core.behaviours import ProfileMixin
from custofeed.users.models import UserWorkProfile


@python_2_unicode_compatible
class Subscriber(ProfileMixin):
    user_work_profile = models.ForeignKey(UserWorkProfile, blank=True, null=True)

    def __str__(self):
        return str(self.get_full_name())


@python_2_unicode_compatible
class Subscription(TimeStampedModel):
    TYPE_CHOICES = (
        (1, _("NFA")),
        (2, _("DAILY REPORT")),
    )
    MEDIUM_CHOICES = (
        (1, _("SMS")),
        (2, _("MAIL")),
    )
    subscriber = models.ForeignKey(Subscriber, blank=True, null=True)
    type = models.IntegerField(_("type"), choices=TYPE_CHOICES, blank=False, null=False)
    medium = models.IntegerField(_("medium"), choices=MEDIUM_CHOICES, blank=False, null=False)
    active = models.BooleanField(_("active"), default=False, blank=False, null=False)

    def __str__(self):
        return str(
            self.subscriber.get_full_name() + ": "
            + self.get_type_display() + " " + self.get_medium_display()
        )

    def get_type_choices(self):
        return self.TYPE_CHOICES

    def get_medium_choices(self):
        return self.MEDIUM_CHOICES

