from django.contrib import admin
from .models import *


@admin.register(Subscriber)
class SubscriberAdmin(admin.ModelAdmin):
    model = Subscriber
    list_display = ['user_work_profile', 'first_name', 'last_name', 'mobile', 'email', 'active']
    list_filter = ['user_work_profile', 'active']
    search_fields = ['user_work_profile', 'active']

    pass


@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    model = Subscription
    list_display = ['subscriber', 'type', 'medium', 'active']
    list_filter = ['type', 'medium', 'active']
    search_fields = list_filter

    pass
