# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from custofeed.companies import views as companies_views
from custofeed.feedbacks import views as feedbacks_views

from . import views, chart_views

urlpatterns = [
    url(r'^$', views.b2b_dashboard, name='b2b_dashboard'),
    url(r'^activate/$', views.b2b_redirect_activation, name='b2b_redirect_activation'),
    url(r'^register/$', views.b2b_register, name='b2b_register'),
    url(r'^feedback-forms/$', views.b2b_feedback_forms, name='b2b_feedback_forms'),
    url(r'^feedback-forms/creator/$', views.b2b_feedback_form_creator, name='b2b_feedback_form_creator'),
    url(r'^feedback-forms/customizer/$', views.b2b_feedback_form_customizer, name='b2b_feedback_form_customizer'),
    url(r'^feedback-forms/customizer/(?P<pk>[0-9]+)/$', views.b2b_feedback_form_customizer_questions,
        name='b2b_feedback_form_customizer_questions'),
    url(r'^feedbacks/$', views.b2b_feedbacks, name='b2b_feedbacks'),
    url(r'^feedbacks/(?P<pk>[0-9]+)/$', views.b2b_feedback, name='b2b_feedback'),
    url(r'^employees/$', views.b2b_feedback_takers, name='b2b_feedback_takers'),
    url(r'^employees/(?P<pk>[0-9]+)/$', views.b2b_feedback_taker, name='b2b_feedback_taker'),
    url(r'^employees/edit/(?P<pk>[0-9]+)/$', feedbacks_views.b2b_feedback_taker_edit, name='b2b_feedback_taker_edit'),
    url(r'^customers/$', views.b2b_feedbackers, name='b2b_feedbackers'),
    url(r'^customers/(?P<pk>[0-9]+)/$', views.b2b_feedbacker, name='b2b_feedbacker'),
    url(r'^company/$', views.b2b_company, name='b2b_company'),
    url(r'^company/edit/$', companies_views.b2b_company_edit, name='b2b_company_edit'),
    url(r'^stores/$', views.b2b_stores, name='b2b_stores'),
    url(r'^stores/edit/(?P<pk>[0-9]+)/$', companies_views.b2b_store_edit, name='b2b_store_edit'),
    url(r'^feedback-request/$', views.b2b_feedback_request, name='b2b_feedback_request'),
    url(r'^feedbacker-referral/referrer-access/2/$', views.b2b_referral, name='b2b_referral'),
    url(r'^orders/$', views.b2b_orders, name='b2b_orders'),
    url(r'^orders/(?P<pk>[0-9]+)/$', views.b2b_order, name='b2b_order'),
    url(r'^subscriptions/$', views.b2b_subscriptions, name='b2b_subscriptions'),
    url(r'^export/$', views.b2b_export_data, name='b2b_export_data'),
    url(r'^promotions/sms/$', views.b2b_promotions_sms, name='b2b_promotions_sms'),
    url(r'^promotions/sms/(?P<pk>[0-9]+)/$', views.b2b_promotions_sms_feedbackers,
        name='b2b_promotions_sms_feedbackers'),
    url(r'^market/$', views.b2b_market, name='b2b_market'),
    url(r'^market/checkout/$', views.b2b_market_checkout, name='b2b_market_checkout'),
    url(r'^loyalty/$', views.b2b_loyalty, name='b2b_loyalty'),
    url(r'^alerts/premium/$', views.b2b_alert_premium, name='b2b_alert_premium'),

    # CRUD
    url(r'^delete/$', views.b2b_delete, name='b2b_delete'),
    url(r'^activate-obj/$', views.b2b_activate, name='b2b_activate'),
    url(r'^edit/(?P<obj_type>[\w\-]+)/(?P<pk>[0-9]+)/$', views.b2b_edit, name='b2b_edit'),

    # Charts
    url(r'^chart-data/(?P<chart>[NSC])/(?P<scale>[YMDH])/(?P<start>[0-9]+)/(?P<delta>[0-9]+)/$', chart_views.b2b_charts,
        name='b2b_charts'),
    url(r'^chart-data-employee/(?P<pk>[0-9]+)/(?P<chart>[NSC])/(?P<scale>[YMDH])/(?P<start>[0-9]+)/(?P<delta>[0-9]+)/$',
        chart_views.b2b_charts, name='b2b_charts_feedback_taker'),
    url(r'^chart-data-multistore/(?P<chart>[F])/(?P<scale>[YMDH])/(?P<start>[0-9]+)/(?P<delta>[0-9]+)/$',
        chart_views.b2b_charts_multistore, name='b2b_charts_multistore'),
    url(r'^chart-data-sms/(?P<chart>[P])/(?P<scale>[YMDH])/(?P<start>[0-9]+)/(?P<delta>[0-9]+)/$',
        chart_views.b2b_charts, name='b2b_charts_sms'),
    url(r'^chart-data-employees/(?P<chart>[F])/(?P<scale>[YMDH])/(?P<start>[0-9]+)/(?P<delta>[0-9]+)/$',
        chart_views.b2b_charts, name='b2b_charts_feedback_takers'),
]
