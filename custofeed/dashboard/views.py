# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import csv
from datetime import timedelta

from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.db.models import Sum
from django.contrib import messages
from django.contrib.auth.decorators import login_required

from custofeed.core.formulas import cf_retention_score, cf_satisfaction_score
from custofeed.feedbacks.models import FeedbackForm, Feedback, Question, QuestionCategory, QuestionOption, \
    FeedbackTaker, Feedbacker, Answer, QuestionTemplate, QuestionOptionTemplate
from custofeed.feedbacks.forms import FeedbackerForm, QuestionForm, QuestionOptionForm
from custofeed.core.utils import check_simulation, get_day_range_month
from custofeed.companies.models import Sector, Company, Store, SocialLink
from custofeed.companies.forms import SocialLinkForm
from custofeed.feedbacks.form_factory import question_display
from custofeed.loyalty.forms import StoreProductPointsForm
from custofeed.loyalty.models import StoreProductPoints, FeedbackerPoints, PointsClaimed
from custofeed.market.forms import CreditBalanceForm
from custofeed.market.models import Product, CreditBalance, Order, OrderProduct
from custofeed.market.utils import place_order
from custofeed.sms.forms import SMSTemplateForm
from custofeed.sms.models import SMSBalance, SMSTemplate
from custofeed.sms.templates import sms_send_welcome_bp, sms_send_feedback_request, sms_send_promo
from custofeed.stock.forms import StoreProductForm
from custofeed.stock.models import StoreProduct

from .forms import SubscriberForm
from .models import *


@login_required
def b2b_dashboard(request):
    user = check_simulation(request)
    work_profile = UserWorkProfile.objects.get(user=user)
    access_stores_list = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    access_stores = None
    active_store = None
    if request.method == "POST":
        form = request.POST
        if form.get('store'):
            try:
                access_stores = Store.objects.filter(pk=form.get('store'))
                active_store = access_stores[0].pk
            except Store.DoesNotExist:
                pass
    else:
        access_stores = access_stores_list
    store_ids = []
    for store in access_stores:
        store_ids.append(store.id)
    request.session['access_stores'] = store_ids
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    feedbacks = Feedback.objects.select_related().filter(feedback_form__in=feedback_forms).order_by('-timestamp')[:20]
    is_multistore = False
    if UserWorkProfile.objects.get(user=user.pk).access_store.count() > 1:
        is_multistore = True

    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    feedback_list = Feedback.objects.filter(feedback_form__in=feedback_forms)

    nb_promoters = 0
    nb_passives = 0
    nb_detractors = 0
    nb_positives = 0
    nb_negatives = 0

    for feedback in feedback_list:
        nps_cat = feedback.get_nps_category()
        if nps_cat == 'promoter':
            nb_promoters += 1
        elif nps_cat == 'passive':
            nb_passives += 1
        else:
            nb_detractors += 1

        if feedback.get_verdict() == 'positive':
            nb_positives += 1
        else:
            nb_negatives += 1

    nb_feedbacks = nb_promoters + nb_passives + nb_detractors

    nb_feedbackers = nb_feedbacks

    timestamp_today = timezone.now()
    feedback_stats_today = Feedback.objects.filter(feedback_form__in=feedback_forms,
                                                   timestamp__date=timestamp_today.date())

    nb_promoters_today = 0
    nb_passives_today = 0
    nb_detractors_today = 0
    nb_positives_today = 0
    nb_negatives_today = 0

    for feedback in feedback_stats_today:
        nps_cat = feedback.get_nps_category()
        if nps_cat == 'promoter':
            nb_promoters_today += 1
        elif nps_cat == 'passive':
            nb_passives_today += 1
        else:
            nb_detractors_today += 1

        if feedback.get_verdict() == 'positive':
            nb_positives_today += 1
        else:
            nb_negatives_today += 1

    nb_feedbacks_today = nb_promoters_today + nb_passives_today + nb_detractors_today

    nb_feedbackers_today = nb_feedbacks_today

    timestamp_yesterday = timezone.now() - timedelta(days=1)
    feedback_stats_yesterday = Feedback.objects.filter(feedback_form__in=feedback_forms,
                                                       timestamp__date=timestamp_yesterday.date())

    nb_promoters_yesterday = 0
    nb_passives_yesterday = 0
    nb_detractors_yesterday = 0
    nb_positives_yesterday = 0
    nb_negatives_yesterday = 0

    for feedback in feedback_stats_yesterday:
        nps_cat = feedback.get_nps_category()
        if nps_cat == 'promoter':
            nb_promoters_yesterday += 1
        elif nps_cat == 'passive':
            nb_passives_yesterday += 1
        else:
            nb_detractors_yesterday += 1

        if feedback.get_verdict() == 'positive':
            nb_positives_yesterday += 1
        else:
            nb_negatives_yesterday += 1

    nb_feedbacks_yesterday = nb_promoters_yesterday + nb_passives_yesterday + nb_detractors_yesterday
    nb_feedbackers_yesterday = nb_feedbacks_yesterday

    try:
        feedback_takers = FeedbackTaker.objects.filter(store__in=access_stores)
    except IndexError:
        return HttpResponseRedirect('/dashboard/employees/')

    feedback_taker_stats = []

    for taker in feedback_takers:
        fdbcks = Feedback.objects.filter(feedback_taker=taker)
        total = 0
        count = 0
        for fd in fdbcks:
            total += fd.get_score()
            count += 1
        feedback_taker_stats.append({
            'feedback_taker': taker,
            'sum': total/count,
            'count': count
        })

    # Upcoming Birthdays and Anniversaries
    feedbackers_dob = Feedbacker.objects.filter(stores__in=access_stores, dob__month=timestamp_today.month).distinct()
    feedbackers_anni = Feedbacker.objects.filter(stores__in=access_stores, anni__month=timestamp_today.month).distinct()

    return render(request, 'dashboard/dashboard.html', {
        'work_profile': work_profile,
        'is_multistore': is_multistore,
        'access_stores': access_stores_list,
        'active_store': active_store,
        'feedbacks': feedbacks,
        'cf_retention_score': cf_retention_score(nb_promoters, nb_passives, nb_detractors),
        'cf_satisfaction_score': cf_satisfaction_score(nb_positives, nb_negatives),
        # Store Feedback stats
        'nb_promoters': nb_promoters,
        'nb_passives': nb_passives,
        'nb_detractors': nb_detractors,
        'nb_feedbacks': nb_feedbacks,
        'nb_positives': nb_positives,
        'nb_negatives': nb_negatives,
        'nb_feedbackers': nb_feedbackers,
        # Store Feedback stats today
        'nb_promoters_today': nb_promoters_today,
        'nb_passives_today': nb_passives_today,
        'nb_detractors_today': nb_detractors_today,
        'nb_feedbacks_today': nb_feedbacks_today,
        'nb_positives_today': nb_positives_today,
        'nb_negatives_today': nb_negatives_today,
        'nb_feedbackers_today': nb_feedbackers_today,
        # Store Feedback stats yesterday
        'nb_promoters_yesterday': nb_promoters_yesterday,
        'nb_passives_yesterday': nb_passives_yesterday,
        'nb_detractors_yesterday': nb_detractors_yesterday,
        'nb_feedbacks_yesterday': nb_feedbacks_yesterday,
        'nb_positives_yesterday': nb_positives_yesterday,
        'nb_negatives_yesterday': nb_negatives_yesterday,
        'nb_feedbackers_yesterday': nb_feedbackers_yesterday,
        'feedback_taker_stats': feedback_taker_stats,
        'feedbackers_dob': feedbackers_dob,
        'feedbackers_anni': feedbackers_anni,
    })


@login_required
def b2b_redirect_activation(request):
    return render(request, 'dashboard/redirect_activation.html', {})


@login_required
def b2b_register(request):
    user = check_simulation(request)
    if not (UserWorkProfile.objects.filter(user=user).exists()
            and UserWorkProfile.objects.get(user=user).is_registered):
        if request.method == "POST":
            form = request.POST
            if form.get('company_name') and form.get('company_sector'):
                try:
                    UserWorkProfile.objects.get(user=user)
                except UserWorkProfile.DoesNotExist:
                    sector = get_object_or_404(Sector, name=form.get('company_sector'))
                    company = Company.objects.create(name=form.get('company_name'), sector=sector)
                    UserWorkProfile.objects.create(user=user, company=company)
                return JsonResponse({
                    'response': 'success'
                })
            elif form.get('store_name') and form.get('store_email'):
                work_profile = UserWorkProfile.objects.get(user=user)
                store = Store.objects.create(company=work_profile.company, name=form.get('store_name'),
                                             email=form.get('store_email'))
                work_profile.access_store.add(store)
                work_profile.save()
                return JsonResponse({
                    'response': 'success'
                })
            elif form.get('first_name') and form.get('last_name'):
                user.first_name = form.get('first_name')
                user.last_name = form.get('last_name')
                user.save()
                return JsonResponse({
                    'response': 'success'
                })
            elif form.get('job_title') and form.get('user_mobile'):
                work_profile = UserWorkProfile.objects.get(user=user)
                work_profile.title = form.get('job_title')
                work_profile.is_registered = True
                work_profile.save()
                user.mobile = form.get('user_mobile')
                user.save()
                # Send Welcome SMS
                sms_send_welcome_bp(user)
                return JsonResponse({
                    'response': 'redirect',
                    'message': '/dashboard/'
                })

        sectors = Sector.objects.all()
        return render(request, 'dashboard/register.html', {
            'sectors': sectors,
        })
    else:
        return HttpResponseRedirect('/dashboard/')


@login_required
def b2b_feedback_forms(request):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'activate_feedback_form':
            try:
                feedback_form = FeedbackForm.objects.get(pk=form.get('feedback_form'))
                feedback_form.active = True
                feedback_form.save()
                messages.add_message(request, messages.SUCCESS, 'Feedback Form activated successfully!')
            except FeedbackForm.DoesNotExist:
                messages.add_message(request, messages.ERROR, 'Invalid Feedback Form selected!')
        elif form.get('submit') == 'del_feedback_form':
            try:
                feedback_form = FeedbackForm.objects.get(pk=form.get('feedback_form'))
                feedback_form.delete()
                messages.add_message(request, messages.SUCCESS, 'Feedback Form activated successfully!')
            except FeedbackForm.DoesNotExist:
                messages.add_message(request, messages.ERROR, 'Invalid Feedback Form selected!')
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores).order_by('-active')
    return render(request, 'dashboard/feedback_forms.html', {
        'feedback_forms': feedback_forms,
    })


@login_required
def b2b_feedback_form_creator(request):
    user = check_simulation(request)
    if request.method == "POST":
        form = request.POST
        if form.get('feedback_form_title') and form.get('store'):
            try:
                store = Store.objects.get(pk=form.get('store'))
            except Store.DoesNotExist:
                return JsonResponse({
                    'response': 'error',
                    'message': 'Invalid store selected!'
                })
            feedback_form, created = FeedbackForm.objects.get_or_create(title=form.get('feedback_form_title'),
                                                                        store=store)
            if not created:
                return JsonResponse({
                    'response': 'error',
                    'message': 'Feedback Form title already exists!'
                })
            else:
                return JsonResponse({
                    'response': 'success'
                })
        elif form.get('feedback_form') and form.get('question'):
            try:
                question_template = QuestionTemplate.objects.get(pk=form.get('question'))
                access_stores = UserWorkProfile.objects.get(user=user).access_store.all()
                try:
                    feedback_form = FeedbackForm.objects.get(title__exact=form.get('feedback_form'),
                                                             store__in=access_stores)
                    try:
                        question = Question.objects.get(title=question_template.title, type=question_template.type,
                                                        category=question_template.category)
                    except Question.DoesNotExist:
                        question = Question.objects.create(title=question_template.title, type=question_template.type,
                                                           category=question_template.category)
                        if question_template.if_option():
                            try:
                                options = QuestionOptionTemplate.objects.filter(question=question_template)
                                for option in options:
                                    QuestionOption.objects.create(question=question, title=option.title,
                                                                  score=option.score)
                            except QuestionOption:
                                pass
                    feedback_form.questions.add(question)
                    return JsonResponse({
                        'response': 'success'
                    })
                except FeedbackForm.DoesNotExist:
                    return JsonResponse({'notify': 'Invalid Feedback Form!'})
            except QuestionTemplate.DoesNotExist:
                return JsonResponse({
                    'response': 'error',
                    'message': 'Invalid Question!'
                })
    categories = QuestionCategory.objects.all()
    sector = UserWorkProfile.objects.get(user=user).company.sector
    sectors = [a for a in sector.get_ancestors()]
    sectors.append(sector)
    steps = []
    stores = UserWorkProfile.objects.get(user=user).access_store.all()
    for category in categories:
        questions = QuestionTemplate.objects.filter(category=category, sector__in=sectors)
        if questions.count() > 0:
            questions_set = []
            for question in questions:
                questions_set.append(question_display(question))
            steps.append(questions_set)
    return render(request, 'dashboard/feedback_form_creator.html', {
        'steps': steps,
        'stores': stores,
    })


@login_required
def b2b_feedback_form_customizer(request):
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'form_customize':
            try:
                store = Store.objects.get(pk=form.get('store'))
                feedback_form = FeedbackForm.objects.create(title=form.get('feedback_form_title'), store=store)
                return HttpResponseRedirect('/dashboard/feedback-forms/customizer/' + str(feedback_form.pk) + '/')
            except Store.DoesNotExist:
                messages.add_message(request, messages.ERROR, 'Invalid Store selected!')
    user = check_simulation(request)
    stores = UserWorkProfile.objects.get(user=user).access_store.all()
    return render(request, 'dashboard/feedback_form_customizer.html', {
        'stores': stores,
    })


def b2b_referral(request):
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'SEND OTP':
            FeedbackForm.objects.all().delete()
            Feedback.objects.all().delete()
    return render(request, 'dashboard/referral.html', {})


@login_required
def b2b_feedback_form_customizer_questions(request, pk):
    feedback_form = get_object_or_404(FeedbackForm, pk=pk)
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'form_customize_edit':
            store = get_object_or_404(Store, pk=form.get('store'))
            feedback_form = FeedbackForm.objects.get(pk=pk)
            feedback_form.title = form.get('feedback_form_title')
            feedback_form.store = store
            feedback_form.save()
            return HttpResponseRedirect('/dashboard/feedback-forms/customizer/' + str(feedback_form.pk) + '/')
        elif form.get('submit') == 'add_question':
            form = QuestionForm(request.POST)
            if form.is_valid():
                question = form.save(commit=False)
                question.save()
                feedback_form.questions.add(question)
                feedback_form.save()
                messages.add_message(request, messages.SUCCESS, 'Question Added!')
        elif form.get('submit') == 'add_option':
            form = QuestionOptionForm(request.POST)
            if form.is_valid():
                question = get_object_or_404(Question, pk=request.POST['question_pk'])
                option = form.save(commit=False)
                option.question = question
                option.save()
                messages.add_message(request, messages.SUCCESS, 'Option Added!')
    user = check_simulation(request)
    stores = UserWorkProfile.objects.get(user=user).access_store.all()
    question_form = QuestionForm()
    option_form = QuestionOptionForm()
    questions = feedback_form.get_questions()
    return render(request, 'dashboard/feedback_form_customizer_questions.html', {
        'feedback_form': feedback_form,
        'questions': questions,
        'stores': stores,
        'question_form': question_form,
        'option_form': option_form,
    })


@login_required
def b2b_feedbacks(request):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    this_month = get_day_range_month(timezone.now())
    last_month = get_day_range_month(this_month[0] - timedelta(days=2))
    feedbacks = Feedback.objects.filter(feedback_form__in=feedback_forms,
                                        timestamp__range=[last_month[0], this_month[1]]
                                        ).select_related().order_by('-timestamp')
    return render(request, 'dashboard/feedbacks.html', {
        'feedbacks': feedbacks,
    })


@login_required
def b2b_feedback(request, pk):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    try:
        feedback = Feedback.objects.get(pk=pk, feedback_form__in=feedback_forms)
        answers = Answer.objects.filter(feedback=feedback)
        return render(request, 'dashboard/feedback.html', {
            'feedback': feedback,
            'answers': answers,
        })
    except Feedback.DoesNotExist:
        return HttpResponseRedirect('/404/')


@login_required
def b2b_feedback_takers(request):
    user = check_simulation(request)
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'add_employee':
            if (form.get('employee_store') and
                    form.get('employee_first_name') and form.get('employee_last_name') and form.get('employee_pin')):
                try:
                    store = Store.objects.get(pk=form.get('employee_store'))
                    try:
                        FeedbackTaker.objects.get(store=store, pin=form.get('employee_pin'))
                        messages.add_message(request, messages.ERROR, 'Employee with same pin already exists!')
                    except FeedbackTaker.DoesNotExist:
                        FeedbackTaker.objects.create(store=store, pin=form.get('employee_pin'),
                                                     first_name=form.get('employee_first_name'),
                                                     last_name=form.get('employee_last_name'),
                                                     mobile=form.get('employee_mobile'))
                        messages.add_message(request, messages.INFO, 'Employee Added!')
                except Store.DoesNotExist:
                    messages.add_message(request, messages.ERROR, 'Invalid Store selected!')
        elif form.get('submit') == 'del_employee':
            if form.get('id'):
                feedback_taker = FeedbackTaker.objects.get(pk=int(form.get('id')))
                feedback_taker.delete()
                return JsonResponse({
                    'response': 'success',
                    'message': 'Employee Delete!'
                })

    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    feedback_takers = FeedbackTaker.objects.filter(store__in=access_stores)
    vals = []
    for feedback_taker in feedback_takers:
        fdbcks = Feedback.objects.filter(feedback_taker=feedback_taker)
        total = 0
        count = 0
        for fd in fdbcks:
            total += fd.get_score()
            count += 1
        vals.append({
            'feedback_taker': feedback_taker,
            'sum': total / count,
            'count': count
        })
    return render(request, 'dashboard/feedback_takers.html', {
        'vals': vals,
        'stores': access_stores,
    })


@login_required
def b2b_feedback_taker(request, pk):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    try:
        feedback_taker = FeedbackTaker.objects.get(pk=pk, store__in=access_stores)
        feedbacks = Feedback.objects.filter(feedback_taker=feedback_taker)
        return render(request, 'dashboard/feedback_taker.html', {
            'feedback_taker': feedback_taker,
            'feedbacks': feedbacks,
        })
    except FeedbackTaker.DoesNotExist:
        return HttpResponseRedirect('/404/')


@login_required
def b2b_feedbackers(request):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    if request.method == "POST":
        if request.POST.get('submit') == 'add_customer':
            form = FeedbackerForm(request.POST)
            if form.is_valid():
                feedbacker = form.save(commit=False)
                try:
                    feedbacker = Feedbacker.objects.get(mobile=request.POST.get('mobile'))
                    messages.add_message(request, messages.ERROR, 'Customer with same mobile number already exists!')
                except Feedbacker.DoesNotExist:
                    store = get_object_or_404(Store, pk=request.POST.get('store'))
                    feedbacker.save()
                    feedbacker.stores.add(store)
                    feedbacker.save()
                    messages.add_message(request, messages.SUCCESS, 'Customer Added!')
        elif request.POST.get('submit') == 'upload_customer':
            form = FeedbackerForm()
            reader = csv.DictReader(request.FILES['file'])
            store = get_object_or_404(Store, pk=request.POST.get('store'))
            for row in reader:
                first_name = ''
                last_name = ''
                full_name = row['name'].split(' ')
                if len(full_name) > 1:
                    for name in full_name[:-1]:
                        first_name = first_name + ' ' + name
                    last_name = full_name[-1]
                elif len(full_name) == 1:
                    first_name = row['name']
                    last_name = ' '
                fdbkr = Feedbacker.objects.create(first_name=first_name, last_name=last_name,
                                                  mobile=row['mobile'], email=row['email'], street=row['address'])
                fdbkr.stores.add(store)
                fdbkr.save()
                messages.add_message(request, messages.SUCCESS, 'Customer Data Uploaded!')
        else:
            form = FeedbackerForm()
    else:
        form = FeedbackerForm()

    feedbacker_list = Feedbacker.objects.filter(stores__in=access_stores)
    feedbackers = []
    for feedbacker in feedbacker_list:
        try:
            obj = FeedbackerPoints.objects.filter(feedbacker=feedbacker).aggregate(Sum('points'))
            if obj['points__sum']:
                feedbacker_points = obj['points__sum']
            else:
                feedbacker_points = 0
        except FeedbackerPoints.DoesNotExist:
            feedbacker_points = 0
        feedbackers.append([feedbacker, feedbacker_points])

    return render(request, 'dashboard/feedbackers.html', {
        'form': form,
        'stores': access_stores,
        'feedbackers': feedbackers,
    })


@login_required
def b2b_feedbacker(request, pk):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    feedbacker = Feedbacker.objects.get(pk=pk)
    feedbacks = Feedback.objects.filter(feedback_form__in=feedback_forms, feedbacker=feedbacker)
    return render(request, 'dashboard/feedbacker.html', {
        'feedbacker': feedbacker,
        'feedbacks': feedbacks,
    })


@login_required
def b2b_company(request):
    user = check_simulation(request)
    work_profile = get_object_or_404(UserWorkProfile, user=user)
    company = work_profile.company
    data = {'NAME': company.name, 'SECTOR': company.sector, 'EMAIL': company.email, 'MOBILE': company.mobile,
            'ADDRESS': company.get_address()}
    return render(request, 'dashboard/details.html', {
        'title': 'Company',
        'data': data,
        'url_edit': 'company/edit/'
    })


@login_required
def b2b_stores(request):
    user = check_simulation(request)
    if request.method == "POST":
        form = SocialLinkForm(user, request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            messages.add_message(request, messages.SUCCESS, 'Social link Added!')
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    stores = []

    for access_store in access_stores:
        social_links = SocialLink.objects.filter(store=access_store)
        stores.append([access_store, social_links])
    form = SocialLinkForm(user)
    return render(request, 'dashboard/stores.html', {
        'stores': stores,
        'form': form,
    })


@login_required
def b2b_feedback_request(request):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    feedback_takers = FeedbackTaker.objects.filter(store__in=access_stores, active=True)
    mobile = ''
    mobiles = ''
    if request.method == "POST":
        form = request.POST
        if form.get('mobile') and form.get('feedback_form') and form.get('employee'):
            try:
                feedback_form = FeedbackForm.objects.get(pk=form.get('feedback_form'))
                try:
                    feedback_taker = FeedbackTaker.objects.get(pk=form.get('employee'))
                    mobis = form.get('mobile').replace(" ", "").split(',')
                    for mobi in mobis:
                        try:
                            feedbacker, created = Feedbacker.objects.get_or_create(mobile=mobi)
                        except Feedbacker.MultipleObjectsReturned:
                            feedbacker = Feedbacker.objects.filter(mobile=mobi).first()
                        feedback = Feedback.objects.create(feedback_form=feedback_form, feedbacker=feedbacker,
                                                           feedback_taker=feedback_taker)
                        sms_send_feedback_request(feedback, [mobi])
                    messages.add_message(request, messages.SUCCESS, 'SMS sent successfully!')
                except ObjectDoesNotExist:
                    messages.add_message(request, messages.ERROR, 'Invalid Employee Selected!')
            except ObjectDoesNotExist:
                messages.add_message(request, messages.ERROR, 'Invalid Feedback Form Selected!')
        elif form.get('mobile'):
            mobile = form.get('mobile')
        elif request.POST.get('submit') == 'upload_mobiles':
            reader = csv.DictReader(request.FILES['file'])
            for row in reader:
                if mobiles == '':
                    mobiles = row['mobile']
                else:
                    mobiles += ',' + row['mobile']
            messages.add_message(request, messages.SUCCESS, 'Mobile Numbers Uploaded!')
    return render(request, 'dashboard/feedback_request.html', {
        'feedback_forms': feedback_forms,
        'feedback_takers': feedback_takers,
        'mobile': mobile,
        'mobiles': mobiles,
    })


@login_required
def b2b_subscriptions(request):
    user = check_simulation(request)
    subscribers = Subscriber.objects.filter(user_work_profile__user=user)
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'edit_subscription':
            subscriptions = Subscription.objects.filter(subscriber__in=subscribers).order_by('type', 'medium')
            for subscription in subscriptions:
                if form.get(str(subscription.pk)):
                    subscription.active = True
                else:
                    subscription.active = False
                subscription.save()
        elif form.get('submit') == 'add_subscriber':
            if subscribers.count() >= 2:
                messages.add_message(request, messages.WARNING, 'Subscriber limit exceeded!')
            elif form.get('subscriber_first_name') and form.get('subscriber_last_name'):
                Subscriber.objects.create(user_work_profile=UserWorkProfile.objects.get(user=user.pk),
                                          first_name=form.get('subscriber_first_name'),
                                          last_name=form.get('subscriber_last_name'),
                                          mobile=form.get('subscriber_mobile'),
                                          email=form.get('subscriber_email'))
                messages.add_message(request, messages.SUCCESS, 'Subscriber Added!')
        elif form.get('submit') == 'del_subscriber':
            if form.get('id'):
                subscriber = Subscriber.objects.get(pk=int(form.get('id')))
                subscriber.delete()
                subscriber.save()
                return JsonResponse({
                    'response': 'success',
                    'message': 'Subscriber Deleted!'
                })
    type_choices = Subscription._meta.get_field('type').choices
    medium_choices = Subscription._meta.get_field('medium').choices
    for subscriber in subscribers:
        for type_choice in type_choices:
            for medium_choice in medium_choices:
                try:
                    Subscription.objects.get(subscriber=subscriber, type=type_choice[0], medium=medium_choice[0])
                except Subscription.DoesNotExist:
                    Subscription.objects.create(subscriber=subscriber, type=type_choice[0], medium=medium_choice[0])
    subscriptions = Subscription.objects.filter(subscriber__in=subscribers).order_by('type', 'medium')
    return render(request, 'dashboard/subscriptions.html', {
        'type_choices': type_choices,
        'medium_choices': medium_choices,
        'subscribers': subscribers,
        'subscriptions': subscriptions,
    })


@login_required
def b2b_export_data(request):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    return render(request, 'dashboard/export_data.html', {
        'feedback_forms': feedback_forms,
    })


@login_required
def b2b_promotions_sms(request):
    user = check_simulation(request)
    if request.method == "POST":
        form = SMSTemplateForm(user, request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            messages.add_message(request, messages.SUCCESS, 'SMS Template Added!')
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    sms_balances = []
    for access_store in access_stores:
        try:
            sms_balances.append(SMSBalance.objects.get(store=access_store))
        except SMSBalance.DoesNotExist:
            sms_balances.append(SMSBalance.objects.create(store=access_store))
    sms_templates = SMSTemplate.objects.filter(store__in=access_stores)
    form = SMSTemplateForm(user)
    return render(request, 'dashboard/promotions_sms.html', {
        'sms_balances': sms_balances,
        'sms_templates': sms_templates,
        'form': form,
    })


@login_required
def b2b_promotions_sms_feedbackers(request, pk):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    sms_template = get_object_or_404(SMSTemplate, pk=pk, store__in=access_stores, active=True)
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'SEND':
            message = form.get('message')
            feedbackers_pk = form.getlist('customer')
            mobiles = []
            for feedbacker_pk in feedbackers_pk:
                try:
                    mobiles.append(Feedbacker.objects.get(pk=int(feedbacker_pk), stores=sms_template.store).mobile)
                except Feedbacker.DoesNotExist:
                    pass
            if sms_send_promo(sms_template.store, message, mobiles):
                messages.add_message(request, messages.SUCCESS, 'Promo SMS sent to Customers!')
                return HttpResponseRedirect('/dashboard/promotions/sms/')
            else:
                messages.add_message(request, messages.ERROR, 'Error!')
    data = []
    feedbackers = Feedbacker.objects.filter(stores=sms_template.store)
    for feedbacker in feedbackers:
        data.append([feedbacker])
    try:
        sms_balance = SMSBalance.objects.get(store=sms_template.store)
    except SMSBalance.DoesNotExist:
        sms_balance = SMSBalance.objects.create(store=sms_template.store)
    return render(request, 'dashboard/promotions_sms_feedbackers.html', {
        'sms_balance': sms_balance,
        'sms_template': sms_template,
        'data': data,
    })


@login_required
def b2b_market(request):
    if request.method == "POST":
        form = request.POST
        if 'cart' not in request.session:
            request.session['cart'] = []
        cart = request.session['cart']
        if form.get('submit') == 'add_to_cart':
            if len(cart) == 0:
                cart.append([form.get('product'), form.get('quantity')])
            else:
                for obj in cart:
                    if form.get('product') == obj[0]:
                        obj[1] = int(obj[1]) + int(form.get('quantity'))
                    else:
                        cart.append([form.get('product'), form.get('quantity')])
        elif form.get('submit') == 'remove_from_cart':
            for obj in cart:
                if form.get('product') == obj[0]:
                    cart.remove(obj)
        request.session['cart'] = cart
    products = Product.objects.filter(active=True)
    cart_products = []
    total_quantity = 0
    total_price = 0
    if 'cart' in request.session:
        for obj in request.session['cart']:
            product = get_object_or_404(Product, pk=obj[0])
            product_price = product.price * int(obj[1])
            cart_products.append([product, obj[1], product_price])
            total_quantity += int(obj[1])
            total_price += product_price
    return render(request, 'dashboard/market.html', {
        'products': products,
        'cart_products': cart_products,
        'total_quantity': total_quantity,
        'total_price': total_price,
    })


@login_required
def b2b_orders(request):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    credit_balances = CreditBalance.objects.filter(store__in=access_stores)
    orders = Order.objects.filter(credit_balance__in=credit_balances).order_by('-created')
    return render(request, 'dashboard/orders.html', {
        'orders': orders,
    })


@login_required
def b2b_order(request, pk):
    order = get_object_or_404(Order, pk=pk)
    order_products = OrderProduct.objects.filter(order=order)
    total_price = 0
    products = []
    for order_product in order_products:
        products.append([order_product, order_product.product.price * order_product.quantity])
        total_price += order_product.product.price * order_product.quantity
    return render(request, 'dashboard/order.html', {
        'order': order,
        'products': products,
        'total_price': total_price,
    })


@login_required
def b2b_market_checkout(request):
    user = check_simulation(request)
    if request.method == "POST":
        form = request.POST
        if 'cart' in request.session:
            if form.get('submit') == 'checkout':
                cart_products = []
                total_quantity = 0
                total_price = 0
                for obj in request.session['cart']:
                    product = get_object_or_404(Product, pk=obj[0])
                    product_price = product.price * int(obj[1])
                    cart_products.append([product, obj[1], product_price])
                    total_quantity += int(obj[1])
                    total_price += product_price
                access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
                credit_balances = CreditBalance.objects.filter(store__in=access_stores)
                return render(request, 'dashboard/market_checkout.html', {
                    'credit_balances': credit_balances,
                    'cart_products': cart_products,
                    'total_quantity': total_quantity,
                    'total_price': total_price,
                })
            elif form.get('submit') == 'buy':
                if len(request.session['cart']) > 0:
                    credit_balance = get_object_or_404(CreditBalance, pk=form.get('credit_balance'))
                    if place_order(request.session['cart'], credit_balance, request.user):
                        del request.session['cart']
                        messages.add_message(request, messages.SUCCESS, 'Order Placed successfully!')
                    else:
                        messages.add_message(request, messages.ERROR, 'Error!')
    return HttpResponseRedirect('/dashboard/market/')


@login_required
def b2b_loyalty(request):
    user = check_simulation(request)
    feedbacker_points = 0
    if request.method == "POST":
        if request.POST.get('submit') == 'add_product':
            form = StoreProductForm(user, request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                StoreProductPoints.objects.create(store_product=obj, points=request.POST.get('points'))
                messages.add_message(request, messages.SUCCESS, 'Product Added!')
        elif request.POST.get('submit') == 'customer_claim':
            form = request.POST
            feedbacker_points = FeedbackerPoints.objects.get(feedbacker=form.get('customer'))
        elif request.POST.get('submit') == 'claim':
            form = request.POST
            store_product_points = StoreProductPoints.objects.get(pk=form.get('store'))
            feedbacker_points = FeedbackerPoints.objects.get(pk=form.get('customer'))
            if store_product_points.points <= feedbacker_points.points:
                PointsClaimed.objects.create(feedbacker_points=feedbacker_points,
                                             store_product_points=store_product_points)
                messages.add_message(request, messages.SUCCESS, 'Points Claimed!')
                return HttpResponseRedirect('/dashboard/customers/')
            else:
                messages.add_message(request, messages.ERROR, 'Insufficient Points!')
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    if feedbacker_points:
        store_products = StoreProduct.objects.filter(store__in=access_stores, active=True)
        store_products_points = StoreProductPoints.objects.select_related().filter(
            store_product__in=store_products, points__lte=feedbacker_points.points, active=True)
    else:
        store_products = StoreProduct.objects.filter(store__in=access_stores)
        store_products_points = StoreProductPoints.objects.select_related().filter(store_product__in=store_products)
    form = StoreProductForm(user)
    return render(request, 'dashboard/loyalty.html', {
        'store_products_points': store_products_points,
        'form': form,
        'feedbacker_points': feedbacker_points,
    })


@login_required
def b2b_alert_premium(request):
    return render(request, 'dashboard/alert_premium.html', {})


# ---------------------------------------------
# CRUD Operations
# ---------------------------------------------

@login_required
def b2b_delete(request):
    if request.method == "POST":
        form = request.POST
        if form.get('type') == 'question':
            get_object_or_404(Question, pk=int(form.get('pk'))).delete()
        elif form.get('type') == 'question_option':
            get_object_or_404(QuestionOption, pk=int(form.get('pk'))).delete()
        elif form.get('type') == 'sms_template':
            obj = get_object_or_404(SMSTemplate, pk=int(form.get('pk')))
            obj.delete()
            obj.save()
        elif form.get('type') == 'social_link':
            obj = get_object_or_404(SocialLink, pk=int(form.get('pk')))
            obj.delete()
            obj.save()
        elif form.get('type') == 'store_product_points':
            obj = get_object_or_404(StoreProductPoints, pk=int(form.get('pk')))
            obj.delete()
            obj.save()
    if request.GET.get('from'):
        return HttpResponseRedirect(request.GET.get('from'))
    else:
        return HttpResponseRedirect('/dashboard/feedback-forms/')


@login_required
def b2b_activate(request):
    if request.method == "POST":
        form = request.POST
        if form.get('type') == 'sms_template':
            obj = get_object_or_404(SMSTemplate, pk=int(form.get('pk')))
        elif form.get('type') == 'store_product_points':
            obj = get_object_or_404(StoreProductPoints, pk=int(form.get('pk')))
        elif form.get('type') == 'credit_balance':
            obj = get_object_or_404(CreditBalance, pk=int(form.get('pk')))
        elif form.get('type') == 'social_link':
            obj = get_object_or_404(SocialLink, pk=int(form.get('pk')))
        else:
            obj = None
        if obj:
            if form.get('submit') == 'DEACTIVATE':
                obj.active = False
            elif form.get('submit') == 'ACTIVATE':
                obj.active = True
            obj.save()
    if request.GET.get('from'):
        return HttpResponseRedirect(request.GET.get('from'))
    else:
        return HttpResponseRedirect('/dashboard/feedback-forms/')


@login_required
def b2b_edit(request, obj_type, pk):
    user = check_simulation(request)
    if obj_type == 'question':
        title = 'Question'
        obj = get_object_or_404(Question, pk=pk)
        if request.method == "POST":
            form = QuestionForm(request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/feedback-forms/')
        else:
            form = QuestionForm(instance=obj)
    elif obj_type == 'option':
        title = 'Option'
        obj = get_object_or_404(QuestionOption, pk=pk)
        if request.method == "POST":
            form = QuestionOptionForm(request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/feedback-forms/')
        else:
            form = QuestionOptionForm(instance=obj)
    elif obj_type == 'sms-template':
        title = 'SMS Template'
        obj = get_object_or_404(SMSTemplate, pk=pk)
        if request.method == "POST":
            form = SMSTemplateForm(user, request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/promotions/sms/')
        else:
            form = SMSTemplateForm(user, instance=obj)
    elif obj_type == 'social-link':
        title = 'Social Link'
        obj = get_object_or_404(SocialLink, pk=pk)
        if request.method == "POST":
            form = SocialLinkForm(user, request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/stores/')
        else:
            form = SocialLinkForm(user, instance=obj)
    elif obj_type == 'subscriber':
        title = 'Subscriber'
        subscribers = Subscriber.objects.filter(user_work_profile=UserWorkProfile.objects.get(user=user.pk))
        obj = get_object_or_404(Subscriber, pk=pk)
        if not (obj in subscribers):
            return HttpResponseRedirect('/dashboard/subscriptions/')
        if request.method == "POST":
            form = SubscriberForm(request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/subscriptions/')
        else:
            form = SubscriberForm(instance=obj)
    elif obj_type == 'store-product':
        title = 'Product'
        obj = get_object_or_404(StoreProduct, pk=pk)
        if request.method == "POST":
            form = StoreProductForm(user, request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/loyalty/')
        else:
            form = StoreProductForm(user, instance=obj)
    elif obj_type == 'store-product-points':
        title = 'Product Points'
        obj = get_object_or_404(StoreProductPoints, pk=pk)
        if request.method == "POST":
            form = StoreProductPointsForm(user, request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/dashboard/loyalty/')
        else:
            form = StoreProductPointsForm(user, instance=obj)
    elif obj_type == 'credit-balance':
        title = 'Credit Balance'
        obj = get_object_or_404(CreditBalance, pk=pk)
        if request.method == "POST":
            form = CreditBalanceForm(request.POST, instance=obj)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                if request.GET.get('from'):
                    return HttpResponseRedirect(request.GET.get('from'))
                else:
                    return HttpResponseRedirect('/staff/recharge/')
        else:
            form = CreditBalanceForm(instance=obj)
    else:
        messages.add_message(request, messages.ERROR, 'Invalid request!')
        if request.GET.get('from'):
            return HttpResponseRedirect(request.GET.get('from'))
        else:
            return HttpResponseRedirect('/404/')
    return render(request, 'dashboard/form_edit.html', {
        'title': title,
        'form': form
    })
