# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from datetime import timedelta

from django.http import JsonResponse
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from dateutil.relativedelta import relativedelta

from custofeed.users.models import UserWorkProfile
from custofeed.feedbacks.models import QuestionCategory, Feedback, FeedbackForm
from custofeed.core.utils import check_simulation, get_access_stores


@login_required
def b2b_charts(request, chart='N', scale='D', start=0, delta=30):
    start = int(start)
    delta = int(delta)
    data = []
    access_stores = get_access_stores(request)
    feedback_forms = FeedbackForm.objects.filter(store__in=access_stores)
    if chart == 'S' or chart == 'N':
        if scale == 'H':
            timestamp_end = timezone.now() - timedelta(hours=int(start))
            timestamp_start = timestamp_end - timedelta(hours=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + timedelta(hours=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = str(local_timestamp.date()) + " " + str(local_timestamp.time().hour) + ":00 hrs"
                stats = Feedback.objects.filter(feedback_form__in=feedback_forms, timestamp__date=timestamp.date(),
                                                hour=timestamp.time().hour)
                if chart == 'S':
                    nb_negatives = 0
                    nb_positives = 0
                    for stat in stats:
                        if stat.get_verdict() == 'positive':
                            nb_positives += 1
                        else:
                            nb_negatives += 1
                    data.append([local_timestamp, nb_positives, nb_negatives])
                elif chart == 'N':
                    nb_promoters = 0
                    nb_passives = 0
                    nb_detractors = 0
                    for stat in stats:
                        nps_cat = stat.get_nps_category()
                        if nps_cat == 'promoter':
                            nb_promoters += 1
                        elif nps_cat == 'passive':
                            nb_passives += 1
                        else:
                            nb_detractors += 1
                        data.append([local_timestamp, nb_promoters, nb_passives, nb_detractors])
        elif scale == 'D':
            timestamp_end = timezone.now() - timedelta(days=int(start))
            timestamp_start = timestamp_end - timedelta(days=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + timedelta(days=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = local_timestamp.date()
                stats = Feedback.objects.filter(feedback_form__in=feedback_forms, timestamp__date=timestamp.date())
                if chart == 'S':
                    nb_negatives = 0
                    nb_positives = 0
                    for stat in stats:
                        if stat.get_verdict() == 'positive':
                            nb_positives += 1
                        else:
                            nb_negatives += 1
                    data.append([local_timestamp, nb_positives, nb_negatives])
                elif chart == 'N':
                    nb_promoters = 0
                    nb_passives = 0
                    nb_detractors = 0
                    for stat in stats:
                        nps_cat = stat.get_nps_category()
                        if nps_cat == 'promoter':
                            nb_promoters += 1
                        elif nps_cat == 'passive':
                            nb_passives += 1
                        else:
                            nb_detractors += 1
                        data.append([local_timestamp, nb_promoters, nb_passives, nb_detractors])
        elif scale == 'M':
            timestamp_end = timezone.now() - relativedelta(months=int(start))
            timestamp_start = timestamp_end - relativedelta(months=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + relativedelta(months=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = str(local_timestamp.date().year) + "-" + str(local_timestamp.date().month)
                stats = Feedback.objects.filter(feedback_form__in=feedback_forms, timestamp__month=timestamp.month,
                                                timestamp__year=timestamp.year)
                if chart == 'S':
                    nb_negatives = 0
                    nb_positives = 0
                    for stat in stats:
                        if stat.get_verdict() == 'positive':
                            nb_positives += 1
                        else:
                            nb_negatives += 1
                    data.append([local_timestamp, nb_positives, nb_negatives])
                elif chart == 'N':
                    nb_promoters = 0
                    nb_passives = 0
                    nb_detractors = 0
                    for stat in stats:
                        nps_cat = stat.get_nps_category()
                        if nps_cat == 'promoter':
                            nb_promoters += 1
                        elif nps_cat == 'passive':
                            nb_passives += 1
                        else:
                            nb_detractors += 1
                        data.append([local_timestamp, nb_promoters, nb_passives, nb_detractors])
        elif scale == 'Y':
            timestamp_end = timezone.now() - relativedelta(years=int(start))
            timestamp_start = timestamp_end - relativedelta(years=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + relativedelta(years=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = local_timestamp.date().year
                stats = Feedback.objects.filter(feedback_form__in=feedback_forms, timestamp__year=timestamp.year)
                if chart == 'S':
                    nb_negatives = 0
                    nb_positives = 0
                    for stat in stats:
                        if stat.get_verdict() == 'positive':
                            nb_positives += 1
                        else:
                            nb_negatives += 1
                    data.append([local_timestamp, nb_positives, nb_negatives])
                elif chart == 'N':
                    nb_promoters = 0
                    nb_passives = 0
                    nb_detractors = 0
                    for stat in stats:
                        nps_cat = stat.get_nps_category()
                        if nps_cat == 'promoter':
                            nb_promoters += 1
                        elif nps_cat == 'passive':
                            nb_passives += 1
                        else:
                            nb_detractors += 1
                        data.append([local_timestamp, nb_promoters, nb_passives, nb_detractors])
    elif chart == 'C':
        categories = QuestionCategory.objects.all()
        for category in categories:
            timestamp_end = 0
            timestamp_start = 0
            if scale == 'H':
                timestamp_end = timezone.now() - timedelta(hours=int(start))
                timestamp_start = timestamp_end - timedelta(hours=int(delta))
            elif scale == 'D':
                timestamp_end = timezone.now() - timedelta(days=int(start))
                timestamp_start = timestamp_end - timedelta(days=int(delta))
            elif scale == 'M':
                timestamp_end = timezone.now() - relativedelta(months=int(start))
                timestamp_start = timestamp_end - relativedelta(months=int(delta))
            elif scale == 'Y':
                timestamp_end = timezone.now() - relativedelta(years=int(start))
                timestamp_start = timestamp_end - relativedelta(years=int(delta))
            stats = Feedback.objects.filter(store__in=access_stores,
                                            timestamp__date__range=[timestamp_start.date(),
                                                                    timestamp_end.date()]
                                            )
            score_total = 0
            nb_score_answers = 0
            for stat in stats:
                score_total += stat.get_score()
                nb_score_answers += stat.get_answers().count()
            data.append({category.name: [score_total, nb_score_answers]})
    return JsonResponse(data, safe=False)


@login_required
def b2b_charts_multistore(request, chart='F', scale='D', start=0, delta=30):
    user = check_simulation(request)
    start = int(start)
    delta = int(delta)
    data = []
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    store_names = []
    for store in access_stores:
        store_names.append(store.name)
    data.append(store_names)
    if chart == 'F':
        if scale == 'H':
            timestamp_end = timezone.now() - timedelta(hours=int(start))
            timestamp_start = timestamp_end - timedelta(hours=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + timedelta(hours=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = str(local_timestamp.date()) + " " + str(local_timestamp.time().hour) + ":00 hrs"
                store_data = [local_timestamp]
                for store in access_stores:
                    feedback_form = FeedbackForm.objects.filter(store=store)[0]
                    stats = Feedback.objects.filter(feedback_form=feedback_form, timestamp__date=timestamp.date(),
                                                    timestamp__hour=timestamp.time().hour).count()
                    if chart == 'F':
                        store_data.append(stats)
                data.append(store_data)
        elif scale == 'D':
            timestamp_end = timezone.now() - timedelta(days=int(start))
            timestamp_start = timestamp_end - timedelta(days=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + timedelta(days=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = local_timestamp.date()
                store_data = [local_timestamp]
                for store in access_stores:
                    stats = Feedback.objects.filter(store=store, timestamp__date=timestamp.date()).count()
                    if chart == 'F':
                        store_data.append(stats)
                data.append(store_data)
        elif scale == 'M':
            timestamp_end = timezone.now() - relativedelta(months=int(start))
            timestamp_start = timestamp_end - relativedelta(months=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + relativedelta(months=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = str(local_timestamp.date().year) + "-" + str(local_timestamp.date().month)
                store_data = [local_timestamp]
                for store in access_stores:
                    stats = Feedback.objects.filter(store=store, timestamp__month=timestamp.month,
                                                    timestamp__year=timestamp.year).count()
                    if chart == 'F':
                        store_data.append(stats)
                data.append(store_data)
        elif scale == 'Y':
            timestamp_end = timezone.now() - relativedelta(years=int(start))
            timestamp_start = timestamp_end - relativedelta(years=int(delta))
            for i in range(0, delta + 2):
                timestamp = timestamp_start + relativedelta(years=i)
                local_timestamp = timezone.localtime(timestamp)
                local_timestamp = local_timestamp.date().year
                store_data = [local_timestamp]
                for store in access_stores:
                    stats = Feedback.objects.filter(store=store, timestamp__year=timestamp.year).count()
                    if chart == 'F':
                        store_data.append(stats)
                    data.append(store_data)
    return JsonResponse(data, safe=False)
