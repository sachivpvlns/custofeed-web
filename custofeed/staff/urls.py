# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.staff_login, name='staff_login'),
    url(r'^dashboard/$', views.staff_dashboard, name='staff_dashboard'),
    url(r'^users/$', views.staff_users, name='staff_users'),
    url(r'^recharge/$', views.staff_recharge, name='staff_recharge'),
]
