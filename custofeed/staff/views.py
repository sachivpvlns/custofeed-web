# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import datetime

from django.contrib.auth import get_user_model, authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from django.contrib.auth.models import Group
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.utils import timezone

from custofeed.companies.models import Company, Store, Sector
from custofeed.feedbacks.models import Feedback
from custofeed.core.utils import end_simulation, get_day_range_month, get_day_range_week
from custofeed.mails.tasks import send_daily_report
from custofeed.market.forms import RechargeForm, CreditBalanceForm
from custofeed.market.models import CreditBalance, Recharge
from custofeed.sms.templates import sms_send_welcome_bp
from custofeed.users.models import UserWorkProfile


def staff_login(request):
    end_simulation(request)
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active and (bool(user.groups.filter(name='Staff')) or user.is_superuser):
                login(request, user)
                return HttpResponseRedirect('/staff/dashboard/')
            else:
                messages.add_message(request, messages.ERROR, 'Access Denied!')
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Credentials!')
    return render(request, 'account/login.html', {
        'form': AuthenticationForm,
        'staff': True,
    })


@login_required
def staff_dashboard(request):
    if request.method == 'POST':
        if request.POST['submit'] == 'send_daily_report':
            send_daily_report()
    end_simulation(request)
    timestamp = timezone.now()
    timestamp_today_range = [datetime.datetime(timestamp.year, timestamp.month, timestamp.day, 0, 0),
                             datetime.datetime(timestamp.year, timestamp.month, timestamp.day, 23, 59)]

    timestamp_yesterday = timezone.now() - datetime.timedelta(days=1)
    timestamp_yesterday_range = [datetime.datetime(timestamp_yesterday.year,
                                                   timestamp_yesterday.month,
                                                   timestamp_yesterday.day, 0, 0),
                                 datetime.datetime(timestamp_yesterday.year,
                                                   timestamp_yesterday.month,
                                                   timestamp_yesterday.day, 23, 59)]

    date_week_range = get_day_range_week(timestamp.date())
    timestamp_week_range = [datetime.datetime(date_week_range[0].year,
                                              date_week_range[0].month,
                                              date_week_range[0].day, 0, 0),
                            datetime.datetime(date_week_range[1].year,
                                              date_week_range[1].month,
                                              date_week_range[1].day, 23, 59)]

    date_month_range = get_day_range_month(timestamp.date())
    timestamp_month_range = [datetime.datetime(date_month_range[0].year,
                                               date_month_range[0].month,
                                               date_month_range[0].day, 0, 0),
                             datetime.datetime(date_month_range[1].year,
                                               date_month_range[1].month,
                                               date_month_range[1].day, 23, 59)]
    # FEEDBACKS ###########
    nb_feedbacks_total = Feedback.objects.all().count()
    nb_feedbacks_today = Feedback.objects.filter(created__range=timestamp_today_range).count()
    nb_feedbacks_yesterday = Feedback.objects.filter(created__range=timestamp_yesterday_range).count()
    nb_feedbacks_week = Feedback.objects.filter(created__range=timestamp_week_range).count()
    nb_feedbacks_month = Feedback.objects.filter(created__range=timestamp_month_range).count()

    # COMPANIES ###########
    nb_companies_total = Company.objects.all().count()
    nb_companies_today = Company.objects.filter(created__range=timestamp_today_range).count()
    nb_companies_yesterday = Company.objects.filter(created__range=timestamp_yesterday_range).count()
    nb_companies_week = Company.objects.filter(created__range=timestamp_week_range).count()
    nb_companies_month = Company.objects.filter(created__range=timestamp_month_range).count()

    # FEEDBACKERS ###########
    nb_feedbackers_total = nb_feedbacks_total
    nb_feedbackers_today = nb_feedbacks_today
    nb_feedbackers_yesterday = nb_feedbacks_yesterday
    nb_feedbackers_week = nb_feedbacks_week
    nb_feedbackers_month = nb_feedbacks_month

    return render(request, 'staff/dashboard.html', {
        'nb_feedbacks_total': nb_feedbacks_total,
        'nb_feedbacks_today': nb_feedbacks_today,
        'nb_feedbacks_yesterday': nb_feedbacks_yesterday,
        'nb_feedbacks_month': nb_feedbacks_month,
        'nb_feedbacks_week': nb_feedbacks_week,

        'nb_companies_total': nb_companies_total,
        'nb_companies_today': nb_companies_today,
        'nb_companies_yesterday': nb_companies_yesterday,
        'nb_companies_month': nb_companies_month,
        'nb_companies_week': nb_companies_week,

        'nb_feedbackers_total': nb_feedbackers_total,
        'nb_feedbackers_today': nb_feedbackers_today,
        'nb_feedbackers_yesterday': nb_feedbackers_yesterday,
        'nb_feedbackers_month': nb_feedbackers_month,
        'nb_feedbackers_week': nb_feedbackers_week,
    })


@login_required
def staff_users(request):
    end_simulation(request)
    if request.method == 'POST':
        g = Group.objects.get(name='Company')
        form = request.POST
        if request.POST['submit'] == 'SIMULATE':
            request.session['simulate'] = request.POST['user']
            return HttpResponseRedirect('/dashboard/')
        elif request.POST['submit'] == 'ACTIVATE':
            user = get_object_or_404(get_user_model(), username=request.POST['user'])
            user.is_active = True
            g.user_set.add(user)
            user.save()
        elif request.POST['submit'] == 'DEACTIVATE':
            user = get_object_or_404(get_user_model(), username=request.POST['user'])
            user.is_active = False
            g.user_set.remove(user)
            user.save()
        elif form.get('submit') == 'register_user':
            try:
                get_user_model().objects.get(username=form.get('username'))
                messages.add_message(request, messages.INFO, 'User already exists!')
            except get_user_model().DoesNotExist:
                try:
                    sector = Sector.objects.get(pk=form.get('sector'))
                    user = get_user_model().objects.create_user(
                        username=form.get('username'), password=form.get('password'), first_name=form.get('first_name'),
                        last_name=form.get('last_name'), mobile=form.get('mobile'), is_active=True
                    )
                    g.user_set.add(user)
                    company = Company.objects.create(name=form.get('company_name'), sector=sector)
                    work_profile = UserWorkProfile.objects.create(user=user, company=company,
                                                                  title=form.get('job_title'), is_registered=True)

                    store = Store.objects.create(company=company, name=form.get('store_name'),
                                                 email=form.get('store_email'))
                    work_profile.access_store.add(store)
                    work_profile.save()

                    # Send Welcome SMS
                    sms_send_welcome_bp(user)
                except Sector.DoesNOtExist:
                    messages.add_message(request, messages.ERROR, 'Invalid Sector!')

    users = get_user_model().objects.all()
    user_accounts = []
    for user in users:
        if not user.is_superuser:
            active = False
            if user.is_active and bool(user.groups.filter(name='Company')):
                active = True
            user_accounts.append({'user': user, 'active': active})
    sectors = Sector.objects.all()
    return render(request, 'staff/users.html', {
        'user_accounts': user_accounts,
        'sectors': sectors,
    })


@login_required
def staff_recharge(request):
    end_simulation(request)
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'add_credit_balance':
            form = CreditBalanceForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.save()
                messages.add_message(request, messages.SUCCESS, 'Credit Balance Added!')
        elif form.get('submit') == 'recharge':
            form = RechargeForm(request.POST)
            if form.is_valid():
                obj = form.save(commit=False)
                obj.recharge_by = request.user
                obj.save()
                messages.add_message(request, messages.SUCCESS, 'Recharge Done!')
    form = CreditBalanceForm()
    recharge_form = RechargeForm()
    credit_balances = CreditBalance.objects.all()
    recharges = Recharge.objects.filter(recharge_by=request.user).order_by('-created')
    return render(request, 'staff/recharge.html', {
        'form': form,
        'recharge_form': recharge_form,
        'credit_balances': credit_balances,
        'recharges': recharges,
    })
