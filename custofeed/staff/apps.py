from __future__ import unicode_literals

from django.apps import AppConfig


class StaffConfig(AppConfig):
    name = 'custofeed.staff'
    verbose_name = "Staff"
