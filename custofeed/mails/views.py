# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def daily_report(request):
    obj = 'test'
    return render(request, 'mails/daily_report.html', obj)
