# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from custofeed.taskapp.celery import app
from custofeed.dashboard.models import Subscriber, Subscription


@app.task(bind=True)
def send_daily_report(self):
    users = []
    subscribers = Subscriber.objects.filter(active=True)
    for subscriber in subscribers:
        try:
            Subscription.objects.get(subscriber=subscriber, type=2, medium=2, active=True)
            users.append([subscriber.user_work_profile.user, subscriber])
        except Subscription.DoesNotExist:
            pass
