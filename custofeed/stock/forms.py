from django import forms

from custofeed.users.models import UserWorkProfile

from .models import StoreProduct


class StoreProductForm(forms.ModelForm):

    class Meta:
        model = StoreProduct
        fields = ('store', 'name', 'price', 'quantity', 'validity')

    def __init__(self, user, *args, **kwargs):
        super(StoreProductForm, self).__init__(*args, **kwargs)
        self.fields['store'].queryset = UserWorkProfile.objects.get(user=user).access_store.all()
