# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.utils.encoding import python_2_unicode_compatible

from model_utils.models import TimeStampedModel
from treebeard.mp_tree import MP_Node

from django.db import models
from django.utils.translation import ugettext_lazy as _

from custofeed.companies.models import Store
from custofeed.core.behaviours import StatusMixin


@python_2_unicode_compatible
class StoreProduct(StatusMixin, TimeStampedModel):
    store = models.ForeignKey(Store, blank=False, null=False)
    name = models.CharField(_("name"), max_length=255, null=False, blank=False)
    price = models.FloatField(_("price"), default=0, null=False, blank=False)
    quantity = models.PositiveIntegerField(_("quantity"), default=0, null=False, blank=False)
    validity = models.PositiveIntegerField(_("validity"), default=0, null=False, blank=False,
                                           help_text=_('validity in days'))
    description = models.TextField(_("description"), null=True, blank=True)

    def __str__(self):
        return self.name
