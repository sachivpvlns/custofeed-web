# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import *


@admin.register(StoreProduct)
class StoreProductAdmin(admin.ModelAdmin):
    model = StoreProduct
    list_display = ['store', 'name', 'price', 'quantity', 'validity', 'description']
    list_filter = ['store', ]
    search_fields = list_display

    pass
