from __future__ import unicode_literals


def cf_nps(nb_promoters=0, nb_passives=0, nb_detractors=0):
    nb_feedbacks = nb_promoters + nb_passives + nb_detractors
    if nb_feedbacks > 0:
        return ((nb_promoters/nb_feedbacks * 100) * 2) - 100
    else:
        return 0


def cf_retention_score(nb_promoters=0, nb_passives=0, nb_detractors=0):
    return float("{0:.2f}".format(float(cf_nps(nb_promoters, nb_passives, nb_detractors) / 10)))


def cf_satisfaction_score(nb_positives=0, nb_negatives=0):
    total = float(nb_negatives + nb_positives)
    if total > 0:
        return float("{0:.2f}".format(float(100 * (nb_positives / total)) / 10))
    else:
        return 0.00
