from __future__ import unicode_literals

from django.apps import AppConfig


class CoreConfig(AppConfig):
    name = 'custofeed.core'
    verbose_name = "Core"
