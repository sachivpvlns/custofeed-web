from __future__ import unicode_literals

import re
import time
from random import randint
import csv
from django.http import HttpResponse
from django import template
import random
import string
from django.utils.http import urlquote as django_urlquote
from django.utils.http import urlencode as django_urlencode
from django.utils.datastructures import MultiValueDict
from datetime import timedelta
import calendar
from django.core.exceptions import PermissionDenied
from django.contrib.admin.utils import label_for_field
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model

from custofeed.users.models import UserWorkProfile


def get_day_range_month(date):
    """
    For a date 'date' returns the start and end date for the month of 'date'.

    Month with 31 days:
    date = datetime.date(2011, 7, 27)
    get_day_range_month(date)
    (datetime.date(2011, 7, 1), datetime.date(2011, 7, 31))

    Month with 28 days:
    date = datetime.date(2011, 2, 15)
    get_day_range_month(date)
    (datetime.date(2011, 2, 1), datetime.date(2011, 2, 28))
    """
    first_day = date.replace(day=1)
    last_day = date.replace(day=calendar.monthrange(date.year, date.month)[1])
    return first_day, last_day


def get_day_range_year(date):
    """
    For a date 'date' returns the start and end date for the year of 'date'.
    """
    first_day = date.replace(day=1, month=1)
    last_day = date.replace(day=31, month=12)
    return first_day, last_day


def get_day_range_week(date):
    """
    For a date 'date' returns the start and end date for the week of 'date'.
    """
    first_day = date - timedelta(days=date.weekday())
    last_day = first_day + timedelta(days=6)
    return first_day, last_day


def check_simulation(request):
    if 'simulate' in request.session:
        user = get_object_or_404(get_user_model(), username=request.session['simulate'])
    else:
        user = request.user
    return user


def end_simulation(request):
    if 'simulate' in request.session:
        del request.session['simulate']


def get_access_stores(request):
    from custofeed.companies.models import Store
    if 'access_stores' in request.session:
        access_stores = []
        for store_id in request.session['access_stores']:
            access_stores.append(Store.objects.get(pk=store_id))
    else:
        access_stores = UserWorkProfile.objects.get(user=request.user.pk).access_store.all()
    return access_stores
