from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel


class StatusMixin(models.Model):
    active = models.BooleanField(_("active"), default=True, blank=False, null=False)

    class Meta:
        abstract = True


class MobileMixin(models.Model):
    mobile = models.CharField(_("mobile number"), blank=True, null=True, max_length=10)

    def __str__(self):
        return self.mobile

    class Meta:
        abstract = True


class ProfileMixin(MobileMixin, StatusMixin, TimeStampedModel):
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), max_length=70, blank=True, null=True)

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def __str__(self):
        return self.get_full_name()

    class Meta:
        abstract = True
