# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.utils.translation import ugettext_lazy as _
from treebeard.mp_tree import MP_Node
from django.utils import timezone
from django.core.exceptions import ValidationError
from model_utils.models import TimeStampedModel
from django.core.validators import MinValueValidator, MaxValueValidator

from custofeed.companies.models import Store, Sector
from custofeed.core.behaviours import ProfileMixin, StatusMixin
from custofeed.feedbacks.form_factory import question_display, answer_display


@python_2_unicode_compatible
class QuestionCategory(MP_Node):
    name = models.CharField(max_length=255, db_index=True)
    description = models.TextField(blank=True)

    node_order_by = ['name']

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        names = [a.name for a in self.get_ancestors()]
        names.append(self.name)
        return " > ".join(names)

    class Meta:
        verbose_name_plural = "question categories"


@python_2_unicode_compatible
class QuestionMixin(TimeStampedModel):
    TYPE_CHOICES = (
        (1, _("MCQ")),
        (2, _("MCQM")),
        (3, _("RATING")),
        (4, _("CHAR")),
        (5, _("TEXT")),
        (6, _("NUMBER")),
        (7, _("DATE")),
        (8, _("TIME")),
        (9, _("DATETIME")),
        (10, _("BOOLEAN")),
    )
    title = models.CharField(_("title"), max_length=255, blank=False, null=False)
    type = models.IntegerField(choices=TYPE_CHOICES, blank=False, null=False)
    category = models.ForeignKey(QuestionCategory, blank=False, null=False)

    def __str__(self):
        return self.title

    def if_option(self):
        """
        Check if question can consist of options.
        :return: ``True`` if it can contain options or ``False``
        """
        if self.type == 1 or self.type == 2:
            return True
        else:
            return False

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Question(QuestionMixin):
    def __str__(self):
        return self.title

    def get_options(self):
        """
        Get a list of options related to this question.
        :return: List of options.
        """
        if self.if_option:
            return QuestionOption.objects.filter(question=self)
        else:
            return []


@python_2_unicode_compatible
class QuestionTemplate(QuestionMixin):
    sector = models.ForeignKey(Sector, blank=False, null=False)

    def __str__(self):
        return self.title

    def get_options(self):
        """
        Get a list of options related to this question.
        :return: List of options.
        """
        if self.if_option:
            return QuestionOptionTemplate.objects.filter(question=self)
        else:
            return []


@python_2_unicode_compatible
class QuestionOptionMixin(TimeStampedModel):
    title = models.CharField(_("title"), max_length=255, blank=False, null=False)
    score = models.PositiveIntegerField(_("CF score"), validators=[MinValueValidator(0), MaxValueValidator(5)],
                                        null=False, blank=False, default=0)

    def save(self, *args, **kwargs):
        """
        Makes sure that the ``type`` of parent Question is ``MCQ`` or ``MCQM`` show error.
        """
        if self.question.if_option:
            super(QuestionOptionMixin, self).save(*args, **kwargs)
        else:
            raise ValidationError("Invalid")

    def __str__(self):
        return self.title

    class Meta:
        abstract = True


@python_2_unicode_compatible
class QuestionOption(QuestionOptionMixin):
    question = models.ForeignKey(Question, blank=False, null=False, related_name='options')

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class QuestionOptionTemplate(QuestionOptionMixin):
    question = models.ForeignKey(QuestionTemplate, blank=False, null=False)

    def __str__(self):
        return self.title


@python_2_unicode_compatible
class FeedbackForm(StatusMixin, TimeStampedModel):
    title = models.CharField(_("name"), max_length=255, blank=False, null=False)
    store = models.ForeignKey(Store, related_name='feedback_forms')
    questions = models.ManyToManyField(Question, verbose_name=_("questions"), blank=True)

    def __str__(self):
        return self.title

    def get_questions(self):
        return self.questions.all()

    def get_questions_display(self):
        questions = self.questions.all()
        questions_set = []
        for question in questions:
            questions_set.append(question_display(question))
        return questions_set


@python_2_unicode_compatible
class FeedbackTaker(ProfileMixin):
    store = models.ForeignKey(Store, related_name='feedback_takers')
    pin = models.CharField(_('pin'), max_length=4, blank=False, null=False, help_text=_('4 digit number allowed.'))

    def __str__(self):
        return self.get_full_name()


@python_2_unicode_compatible
class Feedbacker(ProfileMixin):
    dob = models.DateField(_("date of birth"), blank=True, null=True)
    anni = models.DateField(_("date of anniversary"), blank=True, null=True)
    stores = models.ManyToManyField(Store, verbose_name=_("visited stores"), blank=True)

    def __str__(self):
        return self.get_full_name()


@python_2_unicode_compatible
class Feedback(TimeStampedModel):
    feedback_form = models.ForeignKey(FeedbackForm, blank=False, null=False)
    TYPE_CHOICES = (
        (1, _("DINE IN")),
        (2, _("DELIVERY")),
    )
    type = models.IntegerField(_("type"), choices=TYPE_CHOICES, blank=False, null=False, default=1)
    feedback_taker = models.ForeignKey(FeedbackTaker, blank=False, null=False)
    feedbacker = models.ForeignKey(Feedbacker, blank=False, null=False)
    nps = models.PositiveIntegerField(_("net promoter score"), validators=[MinValueValidator(0), MaxValueValidator(10)],
                                      null=False, blank=False, default=0)
    comment = models.TextField(_('comment'), blank=True, null=False)
    timestamp = models.DateTimeField(_("timestamp"), blank=False, null=False, default=timezone.now)
    info_source = models.CharField(_("information source"), max_length=255, blank=True, null=True, default=None)
    table = models.CharField(_("table number"), max_length=255, blank=True, null=True, default=None)
    bill = models.CharField(_("bill number"), max_length=255, blank=True, null=True, default=None)
    amount = models.PositiveIntegerField(_("bill amount"), blank=True, null=True, default=0)
    remarks = models.TextField(_('remarks'), blank=True, null=False)
    completed = models.BooleanField(_("completed"), default=False, blank=False, null=False)

    def __str__(self):
        return self.feedback_form.title

    def get_answers(self):
        return Answer.objects.filter(feedback=self)

    def get_score(self):
        answers = self.get_answers()
        score_total = 0
        for answer in answers:
            score_total += answer.get_score()
        if self.nb_score_answers > 0:
            return float(self.score_total / answers.count())
        else:
            return 0.0

    def get_verdict(self):
        if self.get_score() > 2:
            return "positive"
        else:
            return "negative"

    def get_nps_category(self):
        if self.nps > 8:
            return "promoter"
        elif 8 >= self.nps >= 7:
            return "passive"
        else:
            return "detractor"

    def get_cf_retention_score(self):
        return int(self.nps) - 5

    class Meta:
        verbose_name = "feedback"


@python_2_unicode_compatible
class Answer(TimeStampedModel):
    feedback = models.ForeignKey(Feedback, blank=False, null=False, related_name='answers')
    question = models.ForeignKey(Question, blank=False, null=False)
    option = models.ForeignKey(QuestionOption, blank=True, null=True)
    text = models.TextField(_("answer"), blank=True, null=True)

    def __str__(self):
        if self.option:
            return str(self.option)
        else:
            return str(self.text)

    def get_answer(self):
        if self.option:
            return str(self.option)
        else:
            return str(self.text)

    def get_answer_display(self):
        return answer_display(self)

    def get_verdict(self):
        try:
            if 5 >= int(self.text) >= 0:
                score = int(self.text)
                if score > 2:
                    return "positive"
                else:
                    return "negative"
            return None
        except ValueError:
            return None

    def get_score(self):
        try:
            if 5 >= int(self.text) >= 0:
                return int(self.text)
            else:
                return 0
        except ValueError:
            return 0
