# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.utils.dateparse import parse_date, parse_time, parse_datetime
from django.utils.timezone import get_current_timezone


def question_display(question):
    div = '<div class="question-wrapper"><div class="question">' + question.title + '</div>'
    if question.if_option():
        div += '<ul class="options">'
        options = question.get_options()
        for option in options:
            div += '<li>' + option.title + '</li>'
        div += '</ul>'
    elif question.get_type_display() == "RATING":
        div += '<div class="options"><i class="fa fa-2x fa-star"></i><i class="fa fa-2x fa-star"></i>' \
               '<i class="fa fa-2x fa-star"></i><i class="fa fa-2x fa-star-half-o"></i>' \
               '<i class="fa fa-2x fa-star-o"></i></div>'
    div += '</div>'
    return {
        'display': div,
        'question': question,
    }


def answer_display(answer):
    div = '<div class="question-wrapper"><div class="question">' + answer.question.title + '</div>'
    if answer.question.if_option():
        div += '<ul class="options">'
        opts = answer.question.get_options()
        if answer.question.get_type_display() == "MCQM":
            options = answer.text.split("#")
        else:
            options = [answer.option.title]
        for opt in opts:
            div += '<li'
            if opt.title in options:
                div += ' class="active"'
            div += '>' + opt.title + '</li>'
        div += '</ul>'
    elif answer.question.get_type_display() == "RATING":
        div += '<div class="options">'
        for i in range(int(answer.text)):
            div += '<i class ="fa fa-2x fa-star"></i>'
        div += '</div>'
    elif answer.question.get_type_display() == "BOOLEAN":
        div += '<span class="fa-stack fa-lg">' \
               '<i class="fa fa-circle fa-stack-2x"></i>'
        if answer.text == 1 or answer.text == True or answer.text == "1" or answer.text == "true":
            div += '<i class="fa fa-check fa-stack-1x fa-inverse"></i>'
        else:
            div += '<i class="fa fa-times fa-stack-1x fa-inverse"></i>'
        div += '</span>'
    elif answer.question.get_type_display() == "DATE":
        div += '<div>'+parse_date(answer.text).strftime('%b %d, %Y')+'</div>'
    elif answer.question.get_type_display() == "TIME":
        div += '<div>'+parse_datetime('1990-01-01T'+answer.text).astimezone(get_current_timezone()).strftime('%I:%M %p')+'</div>'
    elif answer.question.get_type_display() == "DATETIME":
        div += '<div>' + \
               parse_datetime(answer.text).astimezone(get_current_timezone()).strftime('%b %d, %Y, %I:%M %p') + '</div>'
    elif (answer.question.get_type_display() == "CHAR"
          or answer.question.get_type_display() == "NUMBER"
          or answer.question.get_type_display() == "TIME"
          or answer.question.get_type_display() == "DATETIME"):
        div += '<div>'+answer.text+'</div>'
    elif answer.question.get_type_display() == "TEXT":
        div += '<div>'+answer.text.replace("\n", "<br/>")+'</div>'
    div += '</div>'
    return div

