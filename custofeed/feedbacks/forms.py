from django import forms

from .models import FeedbackTaker, Feedbacker, Question, QuestionOption


class FeedbackTakerForm(forms.ModelForm):

    class Meta:
        model = FeedbackTaker
        fields = ('store', 'first_name', 'last_name', 'pin', 'mobile')


class FeedbackerForm(forms.ModelForm):

    class Meta:
        model = Feedbacker
        fields = ('first_name', 'last_name', 'mobile', 'email', 'dob', 'anni')


class QuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = ('title', 'category', 'type')


class QuestionOptionForm(forms.ModelForm):

    class Meta:
        model = QuestionOption
        fields = ('title', 'score')
