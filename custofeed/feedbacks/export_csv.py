# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import csv

from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import get_object_or_404
from django.utils import timezone

from custofeed.users.models import UserWorkProfile
from custofeed.companies.models import Store
from custofeed.feedbacks.models import FeedbackForm, Feedback, Answer
from custofeed.core.utils import check_simulation


@login_required
def export_csv_feedback_form_data(request,):
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'EXPORT':
            # Create the HttpResponse object with the appropriate CSV header.
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="somefilename.csv"'
            user = check_simulation(request)

            timestamp_start = form.get('date_start') + "T00:00:00Z"
            timestamp_end = form.get('date_end') + "T23:59:00Z"

            access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
            feedback_form = get_object_or_404(FeedbackForm, pk=form.get('feedback_form'), store__in=access_stores)
            feedbacks = Feedback.objects.select_related().filter(
                feedback_form=feedback_form, timestamp__range=[timestamp_start, timestamp_end]).order_by('-timestamp')
            questions = feedback_form.get_questions()
            writer = csv.writer(response)
            row_heading = ['S. No.', 'Employee', 'Customer Name', 'Email', 'Mobile',
                           'Date of Birth', 'Date of Anniversary', 'Comment', 'Timestamp', 'Table', 'Bill', 'Amount',
                           'Remarks']
            for question in questions:
                row_heading.append(question.title)
            writer.writerow(row_heading)
            i = 0
            for feedback in feedbacks:
                row = []
                i += 1
                row.append(i)
                row.append(feedback.feedback_taker.get_full_name().encode('utf8'))
                row.append(feedback.feedbacker.get_full_name().encode('utf8'))
                if feedback.feedbacker.email:
                    row.append(feedback.feedbacker.email.encode('utf8'))
                else:
                    row.append("")
                row.append(feedback.feedbacker.mobile)
                row.append(feedback.feedbacker.dob)
                row.append(feedback.feedbacker.anni)
                row.append(feedback.comment.encode('utf8'))
                row.append(timezone.localtime(feedback.timestamp))
                if feedback.table:
                    row.append(feedback.table.encode('utf8'))
                else:
                    row.append("")
                if feedback.bill:
                    row.append(feedback.bill.encode('utf8'))
                else:
                    row.append("")
                row.append(feedback.amount)
                if feedback.remarks:
                    row.append(feedback.remarks.encode('utf8'))
                else:
                    row.append("")

                answers = Answer.objects.filter(feedback=feedback)
                for answer in answers:
                    row.append(answer.get_answer().encode('utf8'))
                writer.writerow(row)

            return response


def export_csv(request,):
    if request.method == "POST":
        form = request.POST
        if form.get('submit') == 'EXPORT':
            response = HttpResponse(content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename="sample.csv"'
            writer = csv.writer(response)
            feedback_forms = FeedbackForm.objects.all()
            for feedback_form in feedback_forms:
                feedbacks = Feedback.objects.select_related().filter(feedback_form=feedback_form).order_by('-timestamp')
                questions = feedback_form.get_questions()
                row_heading = ['S. No.', 'Employee', 'Customer Name', 'Email', 'Mobile',
                               'Date of Birth', 'Date of Anniversary', 'Comment', 'Timestamp', 'Table', 'Bill',
                               'Amount', 'Remarks']
                for question in questions:
                    row_heading.append(question.title)
                writer.writerow([feedback_form.title])
                writer.writerow(row_heading)
                i = 0
                for feedback in feedbacks:
                    row = []
                    i += 1
                    row.append(i)
                    row.append(feedback.feedback_taker.get_full_name().encode('utf8'))
                    row.append(feedback.feedbacker.get_full_name().encode('utf8'))
                    if feedback.feedbacker.email:
                        row.append(feedback.feedbacker.email.encode('utf8'))
                    else:
                        row.append("")
                    row.append(feedback.feedbacker.mobile)
                    row.append(feedback.feedbacker.dob)
                    row.append(feedback.feedbacker.anni)
                    row.append(feedback.comment.encode('utf8'))
                    row.append(timezone.localtime(feedback.timestamp))
                    if feedback.table:
                        row.append(feedback.table.encode('utf8'))
                    else:
                        row.append("")
                    if feedback.bill:
                        row.append(feedback.bill.encode('utf8'))
                    else:
                        row.append("")
                    row.append(feedback.amount)
                    if feedback.remarks:
                        row.append(feedback.remarks.encode('utf8'))
                    else:
                        row.append("")

                    answers = Answer.objects.filter(feedback=feedback)
                    for answer in answers:
                        row.append(answer.get_answer().encode('utf8'))
                    writer.writerow(row)
            return response
