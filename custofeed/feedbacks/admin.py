from django.contrib import admin
from .models import *
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory


@admin.register(FeedbackForm)
class FeedbackFormAdmin(admin.ModelAdmin):
    model = FeedbackForm
    list_display = ['title', 'store', 'active']
    list_filter = ['active', 'store', ]
    search_fields = list_display

    pass


class AnswerInline(admin.TabularInline):
    model = Answer


@admin.register(Feedback)
class FeedbackAdmin(admin.ModelAdmin):
    model = Feedback
    list_display = ['feedback_form', 'completed', 'type', 'feedback_taker', 'feedbacker', 'nps', 'comment',
                    'timestamp', 'table', 'bill', 'amount', 'remarks',
                    ]
    list_filter = ['completed', 'type', 'feedback_form', 'feedback_taker', 'feedbacker', ]
    search_fields = list_display
    inlines = [AnswerInline, ]

    pass


@admin.register(FeedbackTaker)
class FeedbackTakerAdmin(admin.ModelAdmin):
    model = FeedbackTaker
    list_display = ['pin', 'store', 'first_name', 'last_name', 'active']
    list_filter = ['store', 'active']

    pass


@admin.register(Feedbacker)
class FeedbackerAdmin(admin.ModelAdmin):
    model = Feedbacker
    list_display = ['get_full_name', 'mobile', 'email']

    pass


@admin.register(QuestionCategory)
class QuestionCategoryAdmin(TreeAdmin):
    form = movenodeform_factory(QuestionCategory)
    list_display = ('name',)

    pass


class QuestionOptionInline(admin.TabularInline):
    model = QuestionOption


class QuestionOptionTemplateInline(admin.TabularInline):
    model = QuestionOptionTemplate


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    model = Question
    inlines = [QuestionOptionInline, ]
    list_display = ['title', 'type', 'category', ]
    list_filter = ['type', 'category', ]
    search_fields = list_display

    pass


@admin.register(QuestionTemplate)
class QuestionTemplateAdmin(admin.ModelAdmin):
    model = QuestionTemplate
    inlines = [QuestionOptionTemplateInline, ]
    list_display = ['sector', 'title', 'type', 'category', ]
    list_filter = ['sector', 'type', 'category', ]
    search_fields = list_display

    pass


@admin.register(QuestionOption)
class QuestionOptionAdmin(admin.ModelAdmin):
    model = QuestionOption

    pass


@admin.register(QuestionOptionTemplate)
class QuestionOptionTemplateAdmin(admin.ModelAdmin):
    model = QuestionOptionTemplate

    pass


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    model = Answer
    list_display = ['feedback', 'question', 'option', 'text', ]
    list_filter = list_display
    search_fields = list_display

    pass
