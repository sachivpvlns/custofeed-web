# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf.urls import url
from . import views, export_csv

urlpatterns = [
    url(r'^form/(?P<pk>[0-9]+)/$', views.feedback_form, name='feedback_form'),

    # Export CSV
    url(r'^export/csv/feedback-form-data/$', export_csv.export_csv_feedback_form_data,
        name='export_csv_feedback_form_data'),
    url(r'^export/csv/export-all-data/admin-access-only/export-as-csv/$', export_csv.export_csv,
        name='export_csv'),
]
