# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.utils import timezone

from .models import FeedbackTaker, Feedback, Answer, QuestionOption
from .forms import FeedbackTakerForm


@login_required
def b2b_feedback_taker_edit(request, pk):
    feedback_taker = get_object_or_404(FeedbackTaker, pk=pk)
    if request.method == "POST":
        form = FeedbackTakerForm(request.POST, instance=feedback_taker)
        if form.is_valid():
            feedback_taker = form.save(commit=False)
            feedback_taker.save()
            if request.GET.get('from'):
                return HttpResponseRedirect(request.GET.get('from'))
            else:
                return HttpResponseRedirect('/dashboard/employees/')
    else:
        form = FeedbackTakerForm(instance=feedback_taker)
    return render(request, 'dashboard/form_edit.html', {'form': form})


def feedback_form(request, pk):
    feedback = get_object_or_404(Feedback, pk=pk)
    questions = feedback.feedback_form.get_questions()
    store = feedback.feedback_form.store
    if request.method == "POST":
        if not feedback.completed:
            form = request.POST
            if form.get('ret'):
                for question in questions:
                    if question.type == 1:
                        option = QuestionOption.objects.get(pk=int(form.get(str(question.pk))))
                        Answer.objects.create(feedback=feedback, question=question, option=option)
                    elif question.type == 3:
                        Answer.objects.create(feedback=feedback, question=question,
                                              text=int(form.get(str(question.pk))))
                feedback.type = 2  # DELIVERY TYPE
                feedback.nps = form.get('ret')
                feedback.comment = form.get('comment')
                feedback.timestamp = timezone.now()
                feedback.completed = True
                feedback.save()
    return render(request, 'feedbacks/feedback_form.html', {
        'feedback_id': pk,
        'feedback': feedback,
        'store': store,
        'questions': questions
    })
