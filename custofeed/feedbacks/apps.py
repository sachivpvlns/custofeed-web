from __future__ import unicode_literals

from django.apps import AppConfig


class FeedbacksConfig(AppConfig):
    name = 'custofeed.feedbacks'
    verbose_name = "Feedbacks"
