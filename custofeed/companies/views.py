from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required

from custofeed.core.utils import check_simulation
from custofeed.users.models import UserWorkProfile

from .models import Store
from .forms import CompanyForm, StoreForm


@login_required
def b2b_company_edit(request):
    user = check_simulation(request)
    work_profile = get_object_or_404(UserWorkProfile, user=user)
    obj = work_profile.company
    if request.method == "POST":
        form = CompanyForm(request.POST, instance=obj)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            if request.GET.get('from'):
                return HttpResponseRedirect(request.GET.get('from'))
            else:
                return HttpResponseRedirect('/dashboard/company/')
    else:
        form = CompanyForm(instance=obj)
    return render(request, 'dashboard/form_edit.html', {
        'title': 'Company',
        'form': form
    })


@login_required
def b2b_store_edit(request, pk):
    user = check_simulation(request)
    access_stores = UserWorkProfile.objects.get(user=user.pk).access_store.all()
    obj = get_object_or_404(Store, pk=pk)
    if not (obj in access_stores):
        return HttpResponseRedirect('/dashboard/stores/')
    if request.method == "POST":
        form = StoreForm(request.POST, instance=obj)
        if form.is_valid():
            obj = form.save(commit=False)
            obj.save()
            if request.GET.get('from'):
                return HttpResponseRedirect(request.GET.get('from'))
            else:
                return HttpResponseRedirect('/dashboard/stores/')
    else:
        form = StoreForm(instance=obj)
    return render(request, 'dashboard/form_edit.html', {
        'title': 'Store',
        'form': form
    })
