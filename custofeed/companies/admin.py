from django.contrib import admin
from .models import *
from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory


@admin.register(Sector)
class SectorAdmin(TreeAdmin):
    form = movenodeform_factory(Sector)
    list_display = ('name',)

    pass


class StoreInline(admin.TabularInline):
    model = Store
    fieldsets = [(None, {'fields': ['name', 'email', 'mobile']}), ]


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    model = Company
    list_display = ('name', 'sector', 'email')
    fieldsets = [(None, {'fields': ['name', 'sector', 'email', 'mobile']}), ]
    inlines = [StoreInline, ]

    pass


@admin.register(Store)
class StoreAdmin(admin.ModelAdmin):
    model = Store
    list_display = ('company', 'name', 'email')
    fieldsets = [(None, {'fields': ['company', 'name', 'email', 'mobile']}), ]

    pass


@admin.register(SocialLink)
class SocialLinkAdmin(admin.ModelAdmin):
    model = SocialLink
    list_display = ('store', 'website', 'link', 'active')
    list_filter = ['store', 'active']

    pass
