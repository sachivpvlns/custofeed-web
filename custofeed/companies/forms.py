from django import forms

from custofeed.users.models import UserWorkProfile

from .models import Company, Store, SocialLink


class CompanyForm(forms.ModelForm):

    class Meta:
        model = Company
        fields = ('name', 'mobile', 'email', 'sector')


class StoreForm(forms.ModelForm):

    class Meta:
        model = Store
        fields = ('name', 'mobile', 'email')


class SocialLinkForm(forms.ModelForm):

    class Meta:
        model = SocialLink
        fields = ('store', 'website', 'link', 'active')

    def __init__(self, user, *args, **kwargs):
        super(SocialLinkForm, self).__init__(*args, **kwargs)
        self.fields['store'].queryset = UserWorkProfile.objects.get(user=user).access_store.all()
