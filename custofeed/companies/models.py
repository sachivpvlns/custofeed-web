# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.utils.encoding import python_2_unicode_compatible

from django.db import models
from django.utils.translation import ugettext_lazy as _
from treebeard.mp_tree import MP_Node
from model_utils.models import TimeStampedModel
from custofeed.core.behaviours import MobileMixin, StatusMixin


@python_2_unicode_compatible
class Sector(MP_Node):
    name = models.CharField(max_length=255, db_index=True)
    description = models.TextField(blank=True)

    node_order_by = ['name']

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        names = [a.name for a in self.get_ancestors()]
        names.append(self.name)
        return " > ".join(names)


@python_2_unicode_compatible
class Company(MobileMixin, TimeStampedModel):
    name = models.CharField(_("company name"), max_length=255, blank=False, null=False)
    email = models.EmailField(_('email address'), blank=True)
    sector = models.ForeignKey(Sector, blank=False, null=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "companies"


@python_2_unicode_compatible
class Store(MobileMixin, TimeStampedModel):
    company = models.ForeignKey(Company, blank=False, null=False)
    name = models.CharField(_("store name"), max_length=255, blank=False, null=False)
    email = models.EmailField(_('email address'), blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class SocialLink(StatusMixin, TimeStampedModel):
    """
    Social links for store
    """
    store = models.ForeignKey(Store, blank=False, null=False)
    website = models.CharField(_("website"), max_length=255, blank=False, null=False)
    link = models.CharField(_("link"), max_length=255, blank=False, null=False)

    def __str__(self):
        return self.store.name + ' ' + self.website
