# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.utils.encoding import python_2_unicode_compatible

from model_utils.models import TimeStampedModel

from django.db import models
from django.utils.translation import ugettext_lazy as _

from custofeed.companies.models import Store
from custofeed.core.behaviours import StatusMixin
from custofeed.feedbacks.models import Feedbacker
from custofeed.stock.models import StoreProduct


@python_2_unicode_compatible
class FeedbackerPoints(StatusMixin, TimeStampedModel):
    feedbacker = models.ForeignKey(Feedbacker, blank=False, null=False)
    store = models.ForeignKey(Store, blank=False, null=False)
    points = models.PositiveIntegerField(_("points"), blank=False, null=False, default=0)

    class Meta:
        verbose_name = "Feedbacker Points"
        verbose_name_plural = verbose_name
        unique_together = ('feedbacker', 'store',)  # Makes sure that a feedbacker has unique stores

    def __str__(self):
        return self.feedbacker.get_full_name() + ": " + str(self.points)

    def add_feedback_points(self):
        self.points += 10
        self.save()
        return self.points

    def add_referral_points(self):
        self.points += 50
        self.save()
        from custofeed.sms.templates import sms_send_referral_activation
        sms_send_referral_activation(self)
        return self.points


@python_2_unicode_compatible
class StoreProductPoints(StatusMixin, TimeStampedModel):
    store_product = models.ForeignKey(StoreProduct, blank=False, null=False)
    points = models.PositiveIntegerField(_("points"), blank=False, null=False, default=0)

    class Meta:
        verbose_name = "Store Product Points"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.store_product.name + ": " + str(self.points)


@python_2_unicode_compatible
class PointsClaimed(TimeStampedModel):
    feedbacker_points = models.ForeignKey(FeedbackerPoints, blank=False, null=False)
    store_product_points = models.ForeignKey(StoreProductPoints, blank=False, null=False)
    points = models.PositiveIntegerField(_("points"), blank=False, null=False, default=0)

    class Meta:
        verbose_name = "Points Claimed"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.feedbacker_points.feedbacker.get_full_name() + " claimed " + str(self.points) + " for " + \
               self.store_product_points.store_product.name

    def save(self, *args, **kwargs):
        """
        Save points as store_product_points, Update feedbacker_points and store_product quantity
        """
        self.points = self.store_product_points.points
        self.feedbacker_points.points -= self.points
        self.feedbacker_points.save()
        self.store_product_points.store_product.quantity -= 1
        self.store_product_points.store_product.save()
        super(PointsClaimed, self).save(*args, **kwargs)
