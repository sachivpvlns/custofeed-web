# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import *


@admin.register(FeedbackerPoints)
class FeedbackerPointsAdmin(admin.ModelAdmin):
    model = FeedbackerPoints
    list_display = ['feedbacker', 'store', 'points', ]
    list_filter = ['store', 'feedbacker', ]
    search_fields = list_display

    pass


@admin.register(StoreProductPoints)
class StoreProductPointsAdmin(admin.ModelAdmin):
    model = StoreProductPoints
    list_display = ['store_product', 'points', ]
    search_fields = list_display

    pass


@admin.register(PointsClaimed)
class PointsClaimedAdmin(admin.ModelAdmin):
    model = PointsClaimed
    list_display = ['feedbacker_points', 'store_product_points', 'points', 'created']
    list_filter = ['feedbacker_points', 'store_product_points', ]
    search_fields = list_display

    pass
