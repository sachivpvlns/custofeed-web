from django import forms

from custofeed.users.models import UserWorkProfile
from custofeed.stock.models import StoreProduct

from .models import StoreProductPoints


class StoreProductPointsForm(forms.ModelForm):

    class Meta:
        model = StoreProductPoints
        fields = ('store_product', 'points')

    def __init__(self, user, *args, **kwargs):
        super(StoreProductPointsForm, self).__init__(*args, **kwargs)
        stores = UserWorkProfile.objects.get(user=user).access_store.all()
        self.fields['store_product'].queryset = StoreProduct.objects.filter(store__in=stores)
