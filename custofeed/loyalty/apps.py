from __future__ import unicode_literals

from django.apps import AppConfig


class LoyaltyConfig(AppConfig):
    name = 'custofeed.loyalty'
    verbose_name = "Loyalty"
