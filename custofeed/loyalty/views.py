# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.shortcuts import render
from django.http import JsonResponse

from custofeed.users.models import OTPUser
from custofeed.feedbacks.models import Feedbacker

from .models import FeedbackerPoints


def loyalty_login(request):
    verified = False
    if request.method == "POST":
        form = request.POST
        if form.get('authHeader'):
            parsed_data = form.get('authHeader').replace(' ', '').split(',')
            data = {}
            for obj in parsed_data:
                dta = obj.replace('"', '').split('=')
                data[dta[0]] = dta[1]
            try:
                otp_user = OTPUser.objects.get(token=data['oauth_token'])
            except OTPUser.DoesNotExist:
                otp_user = OTPUser.objects.create(token=data['oauth_token'], mobile=form.get('mobile_number'))

            if not otp_user.feedbacker:
                feedbacker = Feedbacker.objects.filter(mobile=otp_user.mobile)
                if feedbacker.exists():
                    otp_user.feedbacker = feedbacker.first()
                    otp_user.save()

            points = FeedbackerPoints.objects.filter(feedbacker=otp_user.feedbacker,
                                                     store__in=otp_user.feedbacker.stores.all())

            body = '<div class="text-center"><h3>Hi ' + otp_user.feedbacker.get_full_name() + '!</h3>'
            if len(points) > 0:
                body += '<p>You have the following points</p><hr/>'
                for point in points:
                    body += (
                        '<h4><small>Store</small> ' + point.store.name + '</h4>' +
                        '<h3>' + str(point.points) + ' <small>points</small></h3></li>'
                    )
            else:
                body += '<h4>No points earned yet!</h4>'
            body += '</div>'

            return JsonResponse({
                'response': body
            })
    return render(request, 'loyalty/login.html', {
        'verified': verified,
    })
