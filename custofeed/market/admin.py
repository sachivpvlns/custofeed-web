# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from treebeard.admin import TreeAdmin
from treebeard.forms import movenodeform_factory

from django.contrib import admin

from .models import *


@admin.register(ProductCategory)
class ProductCategoryAdmin(TreeAdmin):
    form = movenodeform_factory(ProductCategory)
    list_display = ('name',)

    pass


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    model = Product
    list_display = ['name', 'price', 'category', 'quantity', 'validity', 'description']
    list_filter = ['category', ]
    search_fields = list_display

    pass


@admin.register(CreditBalance)
class CreditBalanceAdmin(admin.ModelAdmin):
    model = CreditBalance
    list_display = ['store', 'balance', 'active', ]
    list_filter = ['active', ]
    search_fields = list_display

    pass


class OrderProductInline(admin.TabularInline):
    model = OrderProduct


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    model = Order
    list_display = ['ordered_by', 'amount', 'amount_payed', 'created', 'modified', ]
    list_filter = ['ordered_by', ]
    search_fields = list_display
    inlines = [OrderProductInline, ]

    pass


@admin.register(Recharge)
class RechargeAdmin(admin.ModelAdmin):
    model = Recharge
    list_display = ['amount', 'reference_id', 'credit_balance', 'recharge_by', 'description', 'created', 'modified', ]
    list_filter = ['recharge_by', ]
    search_fields = list_display

    pass
