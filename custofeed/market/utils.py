# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.shortcuts import get_object_or_404

from .models import Product, Order, OrderProduct


def place_order(cart, credit_balance, ordered_by):
    """
    Place an Order.
    """
    if len(cart) > 0:
        order = Order.objects.create(credit_balance=credit_balance, ordered_by=ordered_by)
        total_amount = 0
        for obj in cart:
            product = get_object_or_404(Product, pk=obj[0])
            quantity = int(obj[1])
            OrderProduct.objects.create(order=order, product=product, quantity=quantity)
            total_amount += product.price*quantity
    return False
