# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import
from django.utils.encoding import python_2_unicode_compatible

from model_utils.models import TimeStampedModel
from treebeard.mp_tree import MP_Node

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from custofeed.companies.models import Store
from custofeed.core.behaviours import StatusMixin


@python_2_unicode_compatible
class ProductCategory(MP_Node):
    name = models.CharField(max_length=255, null=False, blank=False, db_index=True)
    description = models.TextField(blank=True)

    node_order_by = ['name']

    def __str__(self):
        return self.full_name

    @property
    def full_name(self):
        names = [a.name for a in self.get_ancestors()]
        names.append(self.name)
        return " > ".join(names)

    class Meta:
        verbose_name = "Product Category"
        verbose_name_plural = "Product Categories"


@python_2_unicode_compatible
class Product(StatusMixin, TimeStampedModel):
    name = models.CharField(_("name"), max_length=255, null=False, blank=False)
    price = models.FloatField(_("price"), default=0, null=False, blank=False)
    category = models.ForeignKey(ProductCategory, blank=False, null=False)
    quantity = models.PositiveIntegerField(_("quantity"), default=0, null=False, blank=False)
    validity = models.PositiveIntegerField(_("validity"), default=0, null=False, blank=False,
                                           help_text=_('validity in days'))
    description = models.TextField(_("description"), null=True, blank=True)

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class CreditBalance(StatusMixin, TimeStampedModel):
    store = models.OneToOneField(Store, blank=False, null=False)
    balance = models.FloatField(_("balance"), null=False, blank=False, default=0)

    class Meta:
        verbose_name = "Credit Balance"
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.store) + ": Rs." + str(self.balance) + '/-'

    def debit(self, val):
        if self.balance - val < 0:
            return False
        else:
            self.balance -= val
            self.save()
            return self.balance

    def credit(self, val):
        self.balance += val
        self.save()
        return self.balance


@python_2_unicode_compatible
class Order(TimeStampedModel):
    credit_balance = models.ForeignKey(CreditBalance, blank=False, null=False)
    ordered_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False)
    amount = models.FloatField(_("amount"), null=False, blank=False, default=0)
    amount_payed = models.FloatField(_("amount_payed"), null=False, blank=False, default=0)

    def __str__(self):
        return self.credit_balance


@python_2_unicode_compatible
class OrderProduct(TimeStampedModel):
    order = models.ForeignKey(Order, null=False, blank=False)
    product = models.ForeignKey(Product, blank=False, null=False)
    quantity = models.PositiveIntegerField(_("quantity"), default=0, null=False, blank=False)

    def __str__(self):
        string = str(self.product) + " for " + str(self.order)
        return string


@python_2_unicode_compatible
class Recharge(TimeStampedModel):
    amount = models.FloatField(_("amount"), default=0, null=False, blank=False)
    reference_id = models.CharField(_("reference ID"), max_length=20, null=True, blank=True)
    credit_balance = models.ForeignKey(CreditBalance, blank=False, null=False)
    recharge_by = models.ForeignKey(settings.AUTH_USER_MODEL, blank=False, null=False)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.reference_id

    def save(self, *args, **kwargs):
        """
        Update Credit Balance
        """
        credit_balance = self.credit_balance
        credit_balance.balance += self.amount
        credit_balance.save()
        super(Recharge, self).save(*args, **kwargs)
