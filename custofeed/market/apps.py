from __future__ import unicode_literals

from django.apps import AppConfig


class MarketConfig(AppConfig):
    name = 'custofeed.market'
    verbose_name = "Market"

    def ready(self):
        import custofeed.market.signals
