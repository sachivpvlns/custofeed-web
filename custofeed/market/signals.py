import pytz
import datetime

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from custofeed.sms.models import SMSBalance

from .models import OrderProduct


@receiver(post_save, sender=OrderProduct, dispatch_uid="post_save_order_product")
def post_save_order_product(sender, **kwargs):
    """
    Update respective balances on Order Product Save
    :param sender:
    :param kwargs:
    :return:
    """
    if kwargs.get('created'):
        order_product = kwargs.get('instance')
        order = order_product.order
        order.amount = order_product.product.price*order_product.quantity
        order.amount_payed = order.amount   # temporarily make amount_payed equal to amount
        order.save()
        if order_product.product.category.name == "PROMOTIONAL SMS":
            store = order.credit_balance.store
            try:
                sms_balance = SMSBalance.objects.get(store=store)
                sms_balance.credit(order_product.quantity)
                sms_balance.save()
            except:
                pass
