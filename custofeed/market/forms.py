# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django import forms

from .models import Recharge, CreditBalance


class CreditBalanceForm(forms.ModelForm):

    class Meta:
        model = CreditBalance
        fields = ('store', 'balance', 'active')


class RechargeForm(forms.ModelForm):

    class Meta:
        model = Recharge
        fields = ('amount', 'reference_id', 'credit_balance', 'description')
