# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from model_utils.models import TimeStampedModel

from django.utils.encoding import python_2_unicode_compatible
from django.db import models
from django.utils.translation import ugettext_lazy as _


from custofeed.core.behaviours import MobileMixin, StatusMixin
from custofeed.feedbacks.models import Store


@python_2_unicode_compatible
class SMSBalance(StatusMixin, TimeStampedModel):
    store = models.OneToOneField(Store, blank=False, null=False)
    balance = models.PositiveIntegerField(_("balance"), null=False, blank=False, default=0)

    class Meta:
        verbose_name = "SMS Balance"
        verbose_name_plural = verbose_name

    def __str__(self):
        return str(self.balance)

    def debit(self, val):
        if self.balance - val < 0:
            return False
        else:
            self.balance -= val
            self.save()
            return self.balance

    def credit(self, val):
        self.balance += val
        self.save()
        return self.balance


@python_2_unicode_compatible
class SMSTemplate(StatusMixin, TimeStampedModel):
    store = models.ForeignKey(Store, blank=False, null=False, related_name='sms_templates')
    title = models.CharField(_("title"), max_length=255, blank=False, null=False)
    message = models.TextField(_("message"), blank=False, null=False)

    def __str__(self):
        return self.message

    class Meta:
        verbose_name = "SMS Template"
        verbose_name_plural = "SMS Templates"
