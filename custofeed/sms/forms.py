from django import forms

from custofeed.users.models import UserWorkProfile

from .models import SMSTemplate


class SMSTemplateForm(forms.ModelForm):

    class Meta:
        model = SMSTemplate
        fields = ('store', 'title', 'message', 'active')

    def __init__(self, user, *args, **kwargs):
        super(SMSTemplateForm, self).__init__(*args, **kwargs)
        self.fields['store'].queryset = UserWorkProfile.objects.get(user=user).access_store.all()
