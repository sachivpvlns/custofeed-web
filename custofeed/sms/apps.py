from __future__ import unicode_literals

from django.apps import AppConfig


class SMSConfig(AppConfig):
    name = 'custofeed.sms'
    verbose_name = "SMS"
