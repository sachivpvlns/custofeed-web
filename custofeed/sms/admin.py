from django.contrib import admin
from .models import *


@admin.register(SMSBalance)
class SMSBalanceAdmin(admin.ModelAdmin):
    model = SMSBalance
    list_display = ['store', 'balance', 'modified', 'created', ]
    list_filter = ['store', ]
    search_fields = list_filter

    pass


@admin.register(SMSTemplate)
class SMSTemplateAdmin(admin.ModelAdmin):
    model = SMSTemplate
    list_display = ['store', 'title', 'message', 'active']
    list_filter = ['store', 'active']
    search_fields = ['store', ]

    pass
