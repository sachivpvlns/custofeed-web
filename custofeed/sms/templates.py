# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import random

from django.utils import timezone

from custofeed.companies.models import SocialLink
from custofeed.users.models import UserWorkProfile

from .models import SMSBalance
from .utils import sms_send


def sms_send_nfa(feedback):
    """
    Send Negative Feedback Alert
    :return:
    """
    feedbacker_name = feedback.feedbacker.get_full_name()
    feedbacker_mobile = feedback.feedbacker.mobile
    comment = feedback.comment
    message = (
        'Dear Business Partner, You have a negative feedback in ' + feedback.feedback_form.store.name +
        ' from ' + feedbacker_name + ' [ ' + feedbacker_mobile + ' ] '
    )
    if not comment == "" and comment:
        message += ('"' + comment + '"')

    contacts = []
    store = feedback.feedback_form.store
    user_workprofiles = UserWorkProfile.objects.filter(access_store=store)
    from custofeed.dashboard.models import Subscriber, Subscription
    subscribers = Subscriber.objects.filter(user_work_profile__in=user_workprofiles, active=True)
    for subscriber in subscribers:
        try:
            Subscription.objects.get(subscriber=subscriber, type=1, medium=1, active=True)
            contacts.append(subscriber.mobile)
        except Subscription.DoesNotExist:
            pass

    if len(contacts) > 0:
        sms_send(message, contacts)


def sms_send_welcome_bp(user):
    """
    Send Welcome message to business partner
    :return:
    """
    message = (
        'Dear Business Partner, Welcome on board to CustoFeed.'
    )
    sms_send(message, [user.mobile])


def sms_send_feedback_taker_pin(feedback_taker):
    """
    Send message to feedback taker about pin
    :return:
    """
    message = (
        feedback_taker.pin + ' is your CustoFeed feedback login PIN.'
    )
    sms_send(message, [feedback_taker.mobile])


def sms_send_thank_you(feedback, loyalty=True):
    """
    Send Thank You SMS to Feedbacker
    :return:
    """
    if not loyalty:
        message = ('Dear ' + feedback.feedbacker.get_full_name() +
                   ', \nThank you for having taken your time to provide us with your valuable feedback, your opinion '
                   'will really help us to improve further. From: ' + feedback.feedback_form.store.name
                   )
    else:
        from custofeed.loyalty.models import FeedbackerPoints
        feedbacker_points = FeedbackerPoints.objects.filter(store=feedback.feedback_form.store,
                                                            feedbacker=feedback.feedbacker)
        if feedbacker_points.exists():
            feedbacker_points = feedbacker_points.first()
        else:
            feedbacker_points = FeedbackerPoints.objects.create(store=feedback.feedback_form.store,
                                                                feedbacker=feedback.feedbacker)
        message = ('Dear ' + feedback.feedbacker.get_full_name() +
                   ', \nThank you for having taken your time to provide us with your valuable feedback. You have '
                   'earned 10 points. Your total balance is ' + str(feedbacker_points.points) +
                   '. Claim exciting freebies on your next visit. \nTo check your points visit: https://goo.gl/21XLpZ'
                   ' \nFrom: ' + feedback.feedback_form.store.name
                   )

    if feedback.get_verdict() == "positive":
        rand_link = None
        links = SocialLink.objects.filter(store=feedback.feedback_form.store)
        if links.exists():
            rand_link = links[random.randrange(len(links))]

        if rand_link:
            if rand_link.website.lower() == 'facebook':
                msg_addon = ' \nLike and check-in us on: ' + rand_link.link
            elif rand_link.website.lower() == 'twitter':
                msg_addon = ' \nFollow us on: ' + rand_link.link
            elif rand_link.website.lower() == 'zomato':
                msg_addon = ' \nRate us on: ' + rand_link.link
            else:
                msg_addon = ' \nVisit us on: ' + rand_link.link
            if msg_addon:
                message += msg_addon
    sms_send(message, [feedback.feedbacker.mobile])


def sms_send_feedback_request(feedback, mobile):
    """
    Send feedback request SMS to given mobile numbers
    :return:
    """
    message = (
        'Dear Customer,\n Hope you had a great experience at ' + feedback.feedback_form.store.name +
        '. Pls click the link below and share ur feedback. Link: http://www.custofeed.com/feedbacks/form/' +
        feedback.pk + '/'
    )
    sms_send(message, mobile)


def sms_send_promo(store, message, mobile):
    """
    Send Promo SMS to given mobile numbers
    :return:
    """
    sms_balance, created = SMSBalance.objects.get_or_create(store=store)
    if sms_balance.debit(len(mobile)):
        sms_send(message, mobile, 2)
        from custofeed.dashboard.models import PromoSMSStatHourly
        PromoSMSStatHourly.objects.create(store=store, nb_mobiles=len(mobile), message=message,
                                          date=timezone.now().date(), hour=timezone.now().time().hour)
        return True
    else:
        return False


def sms_send_referral(referral):
    """
    Send Referral SMS to referee
    :return:
    """
    message = (
        'Hi ' + referral.referee.first_name + ',\n This is ' + referral.get_referrer().get_full_name() +
        ' do visit ' + referral.store.name + ' and get exciting offers.\n '
    )
    rand_link = None
    links = SocialLink.objects.filter(store=referral.store)
    if links.exists():
        rand_link = links[random.randrange(len(links))]

    if rand_link:
        if rand_link.website.lower() == 'facebook':
            msg_addon = ' \nLike and check-in us on: ' + rand_link.link
        elif rand_link.website.lower() == 'twitter':
            msg_addon = ' \nFollow us on: ' + rand_link.link
        elif rand_link.website.lower() == 'zomato':
            msg_addon = ' \nRate us on: ' + rand_link.link
        else:
            msg_addon = ' \nVisit us on: ' + rand_link.link
        if msg_addon:
            message += msg_addon

    sms_send(message, [referral.referee.mobile])
    return True


def sms_send_referral_activation(feedbacker_points):
    """
    Send Referral SMS Activation to referrer
    :return:
    """
    feedbacker = feedbacker_points.feedbacker
    store = feedbacker_points.store
    from custofeed.loyalty.models import FeedbackerPoints
    feedbacker_points = FeedbackerPoints.objects.filter(store=store,
                                                        feedbacker=feedbacker)

    if feedbacker_points.exists():
        feedbacker_points = feedbacker_points.first()
    else:
        feedbacker_points = FeedbackerPoints.objects.create(store=store,
                                                            feedbacker=feedbacker)
    message = (
        'Hi ' + feedbacker_points.get_referrer().first_name + ',\n You have earned 50 points on a referral activation. '
                                                              'Your current balance is ' +
        str(feedbacker_points.points) +
        '. Claim exciting freebies on your next visit. \nTo check your points visit: https://goo.gl/21XLpZ'
        ' \nFrom: ' + store.name
    )

    sms_send(message, [feedbacker.mobile])
    return True
