# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import requests

from django.conf import settings


def sms_send(message, contacts, sms_type=1, sender_id=settings.SMS_SENDER_ID, start_time=''):
    try:
        # Validating mobile numbers
        for contact in contacts:
            contacts.remove(contact)

        if len(contacts) <= 0:
            raise Exception("No number for sending message")

        if sms_type == 1:
            username = settings.SMS_USERNAME
            password = settings.SMS_PASSWORD
        else:
            username = settings.SMS_PROMO_USERNAME
            password = settings.SMS_PROMO_PASSWORD

        # Removing duplicate mobile numbers
        contacts = set(contacts)

        params = {
            'UserId': username,
            'pwd': password,
            'Message': message,
            'Contacts': (', '.join(contacts)),
            'SenderId': sender_id,
            'StartTime': start_time,
        }
        response = requests.get(settings.SMS_BASE_URL + 'SMSHttp.aspx', params=params)

        if not response.status_code == 200:
            raise Exception("Error in sending message")

        responses = str(response.content).split('<!')[0].strip().replace(" ", "").split(';')
        for response in responses:
            objs = response.split(',')
            data = []
            for obj in objs:
                obj = obj.split(':')
                data.append(obj[1])
    except Exception as e:
        print (e.message)


def sms_delivery_report():
    try:
        params = {
            'UserId': settings.SMS_USERNAME,
            'pwd': settings.SMS_PASSWORD,
            'dlrFormat': 1,
        }
        response = requests.get(settings.SMS_BASE_URL + 'DlrReportHttp.aspx', params=params)

        if not response.status_code == 200:
            raise Exception("Error in sending message")

        responses = str(response.content).split('<!')[0].strip().replace(" ", "").split(';')
        return responses
    except Exception as e:
        print (e.message)


def sms_check_balance():
    try:
        params = {
            'UserId': settings.SMS_USERNAME,
            'pwd': settings.SMS_PASSWORD,
            'ServiceType': '',
        }
        response = requests.get(settings.SMS_BASE_URL + 'creditchecker.aspx', params=params)

        if not response.status_code == 200:
            raise Exception("Error in sending message")

        responses = str(response.content).split('<!')[0].strip().replace(" ", "").split(',')
        data = {}
        for response in responses:
            obj = response.split(':')
            data[obj[0]] = obj[1]
        return data
    except Exception as e:
        print (e.message)
